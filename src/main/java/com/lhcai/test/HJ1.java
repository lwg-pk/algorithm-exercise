package com.lhcai.test;

import java.util.Scanner;

/**
 * @author lhcstart
 * @create 2023-02-22 17:06
 */
public class HJ1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int n = sc.nextInt();
            if (n==0){
                return;
            }
            int solve = solve(n);
            System.out.println(solve);
        }
    }

    public static int solve(int n) {
        int nums = 0;
        if (n == 1) {
            return 0;
        }
        if (n == 2) {
            return 1;
        }
        if (n > 2) {
            int a = n / 3;
            int b = n % 3;
            nums = a;
            nums += solve(b + a);
        }
        return nums;
    }
}
