package com.lhcai.lc.other;

/**
 * @author lhcstart
 * @create 2023-02-26 16:25
 */
class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    //前序遍历
    public void preOrder() {

        System.out.println(this);
        if (this.left != null) {
            this.left.preOrder();
        }

        if (this.right!=null){
            this.right.preOrder();
        }
    }
    //中序遍历
    public void infixOrder() {

        if (this.left != null) {
            this.left.infixOrder();
        }
        System.out.println(this);

        if (this.right!=null){
            this.right.infixOrder();
        }
    }
    //后序遍历
    public void postOrder() {

        if (this.left != null) {
            this.left.postOrder();
        }

        if (this.right!=null){
            this.right.postOrder();
        }
        System.out.println(this);
    }

    TreeNode() {
    }

    TreeNode(int val) {
        this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }

}
