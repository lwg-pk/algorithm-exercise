package com.lhcai.lc.other;

/**
 * @author lhcstart
 * @create 2023-02-25 19:36
 */
import java.util.LinkedList;
import java.util.Queue;

/**
 * 腐烂的橘子
 */
public class lc994 {
    public int orangesRotting(int[][] grid) {
        //获取数组的行列
        int N = grid.length;
        int M = grid[0].length;

        Queue<int[]> queue = new LinkedList<>();//存储腐烂的橘子
        int count = 0;//统计新鲜橘子的个数

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                if (grid[i][j] == 1) {
                    count++;
                } else if (grid[i][j] == 2) {
                    queue.add(new int[]{i, j});//腐烂橘子的坐标
                }
            }
        }

        int round = 0;//记录轮数
        while (count > 0 && !queue.isEmpty()) {//有新鲜橘子并且队列中有腐烂橘子时循环
            //轮数+1
            round++;
            int n = queue.size();//获取对列所有烂橘子数，循环

            for (int i = 0; i < n; i++) {
                int[] orange = queue.poll();//从队列中弹出一个烂橘子,获取坐标
                int r = orange[0];
                int c = orange[1];

                //向上
                if (r - 1 >= 0 && grid[r - 1][c] == 1) {
                    count--;//新鲜橘子减一
                    grid[r - 1][c] = 2;//变成烂橘子
                    queue.add(new int[]{r - 1, c});//放入队列
                }
                //向左
                if (c - 1 >= 0 && grid[r][c - 1] == 1) {
                    count--;//新鲜橘子减一
                    grid[r][c - 1] = 2;//变成烂橘子
                    queue.add(new int[]{r, c - 1});//放入队列
                }
                //向下
                if (r + 1 < N && grid[r + 1][c] == 1) {
                    count--;//新鲜橘子减一
                    grid[r + 1][c] = 2;//变成烂橘子
                    queue.add(new int[]{r + 1, c});//放入队列
                }
                //向右
                if (c + 1 <M && grid[r][c + 1] == 1) {
                    count--;//新鲜橘子减一
                    grid[r][c + 1] = 2;//变成烂橘子
                    queue.add(new int[]{r, c + 1});//放入队列
                }
            }
        }

        if (count > 0) {
            return -1;
        } else {
            return round;
        }
    }
}
