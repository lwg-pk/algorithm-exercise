package com.lhcai.lc.other;

import java.util.HashMap;

/**
 * @author lhcstart
 * @create 2023-02-22 21:22
 */

/**
 *
 */
public class lc1839 {
    public static void main(String[] args) {

    }


    public int longestBeautifulSubstring1(String word) {
        int len = 1;//长度
        int type = 1;//种类
        int max = 0;
        for (int i = 1; i < word.length(); i++) {
            if (word.charAt(i) >= word.charAt(i - 1)) {
                len++;
            }
            if (word.charAt(i) > word.charAt(i - 1)) {
                type++;
            }

            if (word.charAt(i) < word.charAt(i - 1)) {
                len = 1;
                type = 1;
            }
            if (type == 5) {
                max = Math.max(max, len);
            }
        }
        return max;
    }

    //**************滑动窗口********
    public int longestBeautifulSubstring2(String word) {
        int n = word.length();
        HashMap<Character, Integer> map = new HashMap<>();
        map.put('a', 0);
        map.put('e', 1);
        map.put('i', 2);
        map.put('o', 3);
        map.put('u', 4);

        char[] ch = word.toCharArray();
        int start = 0, end = 0;
        int res = 0;
        while (end < n - 1) {
            end++;
                if (ch[start] == 'a' && (map.get(ch[end - 1]) + 1 == map.get(ch[end]) || map.get(ch[end - 1]) == map.get(ch[end]))) {
                    if (ch[start] == 'a' && ch[end] == 'u') {
                        res = Math.max(res, end - start + 1);
                    }
                } else {
                    start = end;
                }
            }
        return res;
    }
}
