package com.lhcai.lc.other;

/**
 * @author lhcstart
 * @create 2023-02-26 16:08
 */


import java.util.*;

/**
 * 849. 到最近的人的最大距离
 * 给你一个数组 seats 表示一排座位，其中 seats[i] = 1 代表有人坐在第 i 个座位上，seats[i] = 0 代表座位 i 上是空的（下标从 0 开始）。
 * <p>
 * 至少有一个空座位，且至少有一人已经坐在座位上。
 * <p>
 * 亚历克斯希望坐在一个能够使他与离他最近的人之间的距离达到最大化的座位上。
 * <p>
 * 返回他到离他最近的人的最大距离。
 */
public class lc849 {
    public static void main(String[] args) {
        int i = maxDistToClosest(new int[]{1, 0, 0, 0, 1, 0, 1});
        System.out.println(i);
    }

    public static int maxDistToClosest(int[] seats) {
        int countEnd = 0;
        int countHead = 0;
        int countMax = 0;

        int i = 0;
        int j = seats.length-1;
        // count1记录开头连续0的个数
        while (seats[i] == 0) {
            countHead++;
            i++;
        }
        // count2记录结尾连续0的个数
        while (seats[j] == 0) {
            countEnd++;
            j--;
        }

        // countmax记录从第一个1到最后一个1之间，连续0的最大值
        int countMid = 0;
        for (int k = i+1; k <= j; k++) {
            if (seats[k] == 0) {
                countMid++;
            } else {
                countMax = Math.max(countMax, countMid);
                countMid = 0;
            }
        }

// 返回count1, count2, (countmax+1)/2三者中最大值
        return Math.max(Math.max(countEnd, countHead), (countMax + 1) / 2);
    }
}
