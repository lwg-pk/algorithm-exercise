package com.lhcai.lc.other;




import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author lhcstart
 * @create 2023-02-26 16:09
 */

/**
 * 全排列
 * 给定一个不含重复数字的数组 nums ，返回其 所有可能的全排列 。
 * 你可以 按任意顺序 返回答案。
 */
public class lc46 {
    public List<List<Integer>> permute(int[] nums) {

        List<List<Integer>> ans = new ArrayList<>();
        List<Integer> list = new ArrayList<>();

        for (int num : nums) {
            list.add(num);
        }
        int k = nums.length;
        dfs(ans, list, k, 0);
        return ans;

    }

    /**
     *
     * @param ans 结果集
     * @param list 存放数据的list
     * @param k 数据的个数
     * @param first 使用数据的个数
     */
    public static void dfs(List<List<Integer>> ans, List<Integer> list, int k, int first) {

        // 所有数都填完了
        if (first == k) {
            ans.add(new ArrayList<>(list));
            return;
        }

        for (int i = first; i < k; i++) {
            // 动态维护数组
            Collections.swap(list, first, i);
            // 继续递归填下一个数
            dfs(ans, list, k, first + 1);
            // 撤销操作
            Collections.swap(list, first, i);



        }
    }
}
