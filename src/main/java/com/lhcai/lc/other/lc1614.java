package com.lhcai.lc.other;

/**
 * @author lhcstart
 * @create 2023-02-23 23:01
 */

import java.util.ArrayList;
import java.util.Collections;

/**
 * 括号的最大嵌套深度
 */
public class lc1614 {
    public int maxDepth(String s) {

        int n = s.length();
        int depth = 0;
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < n; i++) {

            if (s.charAt(i) == '(') {
                depth++;
                continue;
            }
            if (s.charAt(i) == ')') {
                list.add(depth);
                depth--;
            }
        }
        Collections.sort(list);
        if (list.size() == 0) {
            return 0;
        } else {
            int max = list.get(list.size() - 1);
            return max;
        }
    }
}

class Solution {
    public int maxDepth(String s) {
        int count = 0;
        int size = 0;
        for(char c:s.toCharArray()){
            if(c=='('){
                ++count;
                size = Math.max(size,count);
            }else if(c==')'){
                --count;
            }
        }

        return size;
    }
}