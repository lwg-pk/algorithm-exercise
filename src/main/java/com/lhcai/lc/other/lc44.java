package com.lhcai.lc.other;


/**
 * @author lhcstart
 * @create 2023-02-26 16:09
 */

/**
 * 通配符
 */
public class lc44 {
    public static void main(String[] args) {
        boolean aa = isMatch("aa", "*");
        System.out.println(String.valueOf(aa));
    }

    public static boolean isMatch(String s, String p) {

        int n = s.length();
        int m = p.length();
        boolean[][] dp = new boolean[n + 1][m + 1];

        //边界条件
        dp[0][0] = true;

        //当s为空的时候，一定是true
        for (int i = 1; i <= m; i++) {
            if (p.charAt(i - 1) == '*') {
                dp[0][i] = true;
            }else {
                break;
            }
        }

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (p.charAt(j - 1) == '*') {
                    dp[i][j] = dp[i][j - 1] || dp[i-1][j];
                } else if (p.charAt(j - 1) == '?') {
                    dp[i][j] = dp[i - 1][j - 1];
                } else if (s.charAt(i - 1) == p.charAt(j - 1)) {
                    dp[i][j] = dp[i - 1][j - 1];
                }
            }
        }

        return dp[n][m];
    }

}

