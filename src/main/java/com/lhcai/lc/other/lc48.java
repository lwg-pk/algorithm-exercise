package com.lhcai.lc.other;

/**
 * @author lhcstart
 * @create 2023-02-26 16:09
 */


/**
 * 旋转图像
 */
public class lc48 {
    public void rotate(int[][] matrix) {

        int n = matrix.length;
        int m = matrix[0].length;
        //水平旋转
        for (int i = 0; i < n/2; i++) {
            for (int j = 0; j < m; j++) {

                int temp = matrix[i][j];
                matrix[i][j] = matrix[n - i - 1][j];
                matrix[n - i - 1][j] = temp;
            }
        }

        //主对角线旋转
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < i; j++) {
                int temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;

            }
        }

    }


    //辅助数组，旋转
    public void rotate1(int[][] matrix) {
        int n = matrix.length;
        int[][] matrix_new = new int[n][n];
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                matrix_new[j][n - i - 1] = matrix[i][j];
            }
        }
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                matrix[i][j] = matrix_new[i][j];
            }
        }
    }
}

