package com.lhcai.lc.other;

/**
 * @author lhcstart
 * @create 2023-02-25 15:14
 */
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 * 从上到下打印二叉树
 */
public class JZlc32_1 {
    public int[] levelOrder(TreeNode root) {

        //如果为空，没有节点
        if (root == null) {
            return new int[0];
        }
        //队列，存放二叉树
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        //list存放遍历的结果
        ArrayList<Integer> ans = new ArrayList<>();

        while (!queue.isEmpty()) {
            //队列弹出一个节点
            TreeNode node = queue.poll();
            //list添加节点的值
            ans.add(node.val);
            //节点左侧有子节点
            if (node.left != null) {
                queue.add(node.left);
            }
            //节点右侧有子节点
            if (node.right != null) {
                queue.add(node.right);
            }
        }

        int[] res = new int[ans.size()];
        for (int i = 0; i < ans.size(); i++) {
            res[i] = ans.get(i);
        }
        return res;
    }


    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }
}
