package com.lhcai.lc.other;

/**
 * @author lhcstart
 * @create 2023-02-26 16:05
 */

import javax.swing.plaf.basic.BasicInternalFrameTitlePane;
import java.util.logging.Level;

/**
 * 矩形面积
 * 给你 二维 平面上两个 由直线构成且边与坐标轴平行/垂直 的矩形，请你计算并返回两个矩形覆盖的总面积。
 * <p>
 * 每个矩形由其 左下 顶点和 右上 顶点坐标表示：
 * <p>
 * 第一个矩形由其左下顶点 (ax1, ay1) 和右上顶点 (ax2, ay2) 定义。
 * 第二个矩形由其左下顶点 (bx1, by1) 和右上顶点 (bx2, by2) 定义。
 */
public class lc223 {
    public int computeArea(int ax1, int ay1, int ax2, int ay2, int bx1, int by1, int bx2, int by2) {
        //A、B的面积
        int areaA = (ax2 - ax1) * (ay2 - ay1);
        int areaB = (bx2 - bx1) * (by2 - by1);

        //重叠部分的长高
        int depth = Math.min(ax2, bx2) - Math.max(ax1, bx1);
        int height = Math.min(ay2, by2) - Math.max(ay1, by1);
        //不重叠的话就小于0，判断与0 的大小确认是否重叠
        depth = Math.max(depth, 0);
        height = Math.max(height, 0);

        return areaA + areaB - depth * height;

    }
}
