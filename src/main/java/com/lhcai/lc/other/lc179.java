package com.lhcai.lc.other;

/**
 * @author lhcstart
 * @create 2023-02-26 16:05
 */
import java.util.Arrays;

/**
 * 最大数
 * 给定一组非负整数 nums，重新排列每个数的顺序（每个数不可拆分）使之组成一个最大的整数
 */
public class lc179 {
    public static void main(String[] args) {
        largestNumber(new int[]{3, 30, 34, 5, 9});
    }

    public static String largestNumber(int[] nums) {

        int n = nums.length;
        String[] ss = new String[n];

        for (int i = 0; i < n; i++) {
            ss[i] = nums[i] + "";
        }

        //排序，降序输出
        Arrays.sort(ss, (a, b) -> {
            String sa = a + b, sb = b + a;
            return sb.compareTo(sa);
        });

        StringBuilder sb = new StringBuilder();
        for (String s : ss) {
            sb.append(s);
        }

        if(sb.charAt(0) =='0'){
            return "0";
        }

        return sb.toString();

    }
}
