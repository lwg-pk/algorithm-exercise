package com.lhcai.lc.other;

/**
 * @author lhcstart
 * @create 2023-02-24 21:16
 */

/**
 * 最长连续递增序列
 */
public class lc674 {
    public static void main(String[] args) {
        lc674 lc = new lc674();
        int i = lc.findLengthOfLCIS(new int[]{1, 3, 5, 4, 7});
        System.out.println(i);
    }

    public int findLengthOfLCIS(int[] nums) {

        if (nums == null || nums.length == 0) {
            return 0;
        }
        int pre = 1; // pre表示dp[i-1]: 必须以i-1位置结尾的递增子数组长度

        int ans = pre;

        for (int i = 1; i < nums.length; i++) {
            int cur = 1;
            if (nums[i] > nums[i - 1]) {
                cur += pre;
            }
            ans = Math.max(cur, ans);
            pre = cur;


        }
        return ans;
    }
}
