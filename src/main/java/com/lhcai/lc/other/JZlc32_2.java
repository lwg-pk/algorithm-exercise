package com.lhcai.lc.other;

/**
 * @author lhcstart
 * @create 2023-02-25 16:47
 */
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * 从上到下按层打印二叉树，同一层的节点按从左到右的顺序打印，每一层打印到一行。
 */
public class JZlc32_2 {

    public List<List<Integer>> levelOrder(TreeNode root) {

        Queue<TreeNode> queue = new LinkedList<>();
        List<List<Integer>> res = new ArrayList<>();
        //添加头结点
        if (root != null) {
            queue.add(root);
        }

        while (!queue.isEmpty()) {
            //定义临时list
            List<Integer> tmp = new ArrayList<>();
            //queue.size()即每层有几个元素
            for (int i = queue.size(); i > 0; i--) {
                //弹出队列中的元素，添加进临时list
                TreeNode node = queue.poll();
                tmp.add(node.val);
                //左不为空
                if (node.left != null) {
                    queue.add(node.left);
                }
                //右不为空
                if (node.right != null) {
                    queue.add(node.right);
                }
            }
            //每次for循环结束，将每层的数据放入结果集
            res.add(tmp);
        }
        return res;
    }

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }
}
