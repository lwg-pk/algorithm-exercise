package com.lhcai.lc.other;

/**
 * @author lhcstart
 * @create 2023-02-26 16:06
 */


/**
 * 给定一个正整数 n，返回 连续正整数满足所有数字之和为 n 的组数 。
 */
public class lc829 {
    public int consecutiveNumbersSum(int n) {

        int ans = 0;
        int bound = 2 * n;

        for (int k = 1; k * (k + 1) <= bound; k++) {
            if (isK(n, k)) {
                ans++;
            }
        }
        return ans;
    }

    /**
     * 如果 k 是奇数，则当 n 可以被 k 整除时，正整数  n 可以表示成k 个连续正整数之和；
     *
     * 如果 k 是偶数，则当  n 不可以被  k 整除且 2n 可以被  k 整除时，正整数  n 可以表示成  k 个连续正整数之和。
     *
     * @param n
     * @param k
     * @return
     */
    public boolean isK(int n, int k) {
        if (k % 2 == 1) {
            return n % k == 0;
        } else {
            return n % k != 0 && 2 * n % k == 0;
        }
    }
}
