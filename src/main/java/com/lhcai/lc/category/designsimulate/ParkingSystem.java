package com.lhcai.lc.category.designsimulate;

import java.util.HashMap;
import java.util.Map;

/**
 * 1603. 设计停车系统
 *
 * @author liuhuaicai
 * @since 2024-10-28 22:13
 */
public class ParkingSystem {
    Map<Integer, Integer> map = new HashMap<>();

    public ParkingSystem(int big, int medium, int small) {
        map.put(1, big);
        map.put(2, medium);
        map.put(3, small);
    }

    public boolean addCar(int carType) {
        Integer integer = map.get(carType);
        if (integer > 0) {
            map.put(carType, integer - 1);
            return true;
        }
        return false;
    }
}
