package com.lhcai.lc.category.designsimulate;

import java.util.HashMap;
import java.util.Map;

/**
 * 1396. 设计地铁系统
 *
 * @author liuhuaicai
 * @since  2024-06-13 21:47
 */
public class UndergroundSystem {
    // 存在多次进站出站的可能


    // 旅客的所有进出记录
    private Map<Integer, Passenger> map;

    public UndergroundSystem() {
        map = new HashMap<>();
    }

    public void checkIn(int id, String stationName, int t) {
        // 判断是否已经存在乘客，存在的话，查看如果有进站，不做操作；
        if (map.containsKey(id)) {
            return;
        }
        Passenger curPassenger = map.getOrDefault(id, new Passenger(id));
        curPassenger.startStation = stationName;
        curPassenger.startTime = t;
        map.put(id, curPassenger);
    }

    // 当前
    public void checkOut(int id, String stationName, int t) {
        // 判断是否已经存在乘客，不存在的话，不做操作；
        if (!map.containsKey(id)) {
            return;
        }
        Passenger curPassenger = map.get(id);
        curPassenger.endStation = stationName;
        curPassenger.endTime = t;
        map.put(id, curPassenger);
    }

    public double getAverageTime(String startStation, String endStation) {
        int tmp = 0;
        int num = 0;
        for (Passenger value : map.values()) {
            if (value.startStation.equals(startStation) && value.endStation.equals(endStation)) {
                num++;
                tmp += value.endTime - value.startTime;
            }
        }
        return 1.0 * tmp / num;
    }

    class Passenger {
        private int id;
        private String startStation;
        private int startTime;
        private String endStation;
        private int endTime;

        public Passenger(int id) {
            this.id = id;
            this.startStation = "";
            this.endStation = "";
        }
    }
}
