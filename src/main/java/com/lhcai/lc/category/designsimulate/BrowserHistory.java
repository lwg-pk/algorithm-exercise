package com.lhcai.lc.category.designsimulate;

import java.util.ArrayList;
import java.util.List;

/**
 * 1472. 设计浏览器历史记录
 *
 * @author liuhuaicai
 * @since  2024-06-12 21:34
 */
public class BrowserHistory {
    List<String> list = new ArrayList<>();

    // 当前页可能会重名
    private String cur;

    // 记录当前索引
    private int index;

    public BrowserHistory(String homepage) {
        list.add(homepage);
        cur = homepage;
        index = 0;
    }

    public void visit(String url) {
        List<String> tmp = list.subList(0, index + 1);
        tmp.add(url);
        list = tmp;
        index = index + 1;
    }

    public String back(int steps) {
        if (steps > index) {
            index = 0;
            cur = list.get(0);
            return cur;
        }
        cur = list.get(index - steps);
        index = index - steps;
        return cur;
    }

    public String forward(int steps) {
        if (steps > list.size() - 1 - index) {
            index = list.size() - 1;
            cur = list.get(list.size() - 1);
            return cur;
        }
        cur = list.get(index + steps);
        index = index + steps;
        return cur;
    }
}
