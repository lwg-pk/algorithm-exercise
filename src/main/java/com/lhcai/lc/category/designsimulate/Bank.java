package com.lhcai.lc.category.designsimulate;

import java.util.HashMap;
import java.util.Map;

/**
 * 2043. 简易银行系统
 *
 * @author liuhuaicai
 * @since  2024-06-12 21:09
 */
class Bank {
    private Map<Integer, Long> map = new HashMap<>();

    public Bank(long[] balance) {
        for (int i = 0; i < balance.length; i++) {
            map.put(i + 1, balance[i]);
        }
    }

    public boolean transfer(int account1, int account2, long money) {
        if (!map.containsKey(account1) || !map.containsKey(account2)) {
            return false;
        }
        Long account1Balance = map.get(account1);
        if (account1 == account2) {
            return account1Balance >= money;
        }
        if (account1Balance < money) {
            return false;
        }
        Long account2Balance = map.get(account2);
        Long account1Cur = account1Balance - money;
        Long account2Cur = account2Balance + money;
        map.put(account1, account1Cur);
        map.put(account2, account2Cur);
        return true;
    }

    public boolean deposit(int account, long money) {
        if (!map.containsKey(account)) {
            return false;
        }
        Long accountBalance = map.get(account);
        map.put(account, accountBalance + money);
        return true;
    }

    public boolean withdraw(int account, long money) {
        if (!map.containsKey(account)) {
            return false;
        }
        Long accountBalance = map.get(account);
        if (accountBalance < money) {
            return false;
        }
        map.put(account, accountBalance - money);
        return true;
    }
}
