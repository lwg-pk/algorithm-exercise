package com.lhcai.lc.category.designsimulate;

import java.util.HashSet;
import java.util.Set;

/**
 * 874. 模拟行走机器人
 * 把坐标范围看成一个二维矩阵。
 * 假设矩阵规模为nm，那么可以将坐标映射为xm+y
 * 这是一种常见的哈希表存储二元数的技巧
 *
 * @author liuhuaicai
 * @since 2024-11-06 20:31
 */
public class Lc874 {
    // 无限大小

    public int robotSim(int[] commands, int[][] obstacles) {
        // 定义方向数组
        int[][] dir = new int[][]{{-1, 0}, {0, 1}, {1, 0}, {0, -1}};
        // 进行坐标映射
        Set<Integer> set = new HashSet<>();
        for (int[] obstacle : obstacles) {
            // 把坐标范围看成一个二维矩阵 假设矩阵规模为nm，那么可以将坐标映射为xm + y
            set.add(obstacle[0] * 60001 + obstacle[1]);
        }
        int d = 1;
        int x = 0;
        int y = 0;
        int res = 0;
        for (int command : commands) {
            if (command < 0) {
                d += command == -1 ? 1 : -1;
                d = (d + 4) % 4;
                continue;
            }
            for (int i = 0; i < command; i++) {
                // 此坐标是障碍点
                if (set.contains((x + dir[d][0]) * 60001 + y + dir[d][1])) {
                    break;
                }
                x += dir[d][0];
                y += dir[d][1];
                res = Math.max(res, x * x + y * y);
            }
        }
        return res;
    }
}
