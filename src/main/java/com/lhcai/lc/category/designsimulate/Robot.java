package com.lhcai.lc.category.designsimulate;


/**
 * 2069. 模拟行走机器人 II
 * 1、寻找规律：机器人绕外圈行走，则可以根据 机器人行走的总步数 对  外圈总长度 进行 取模 ，此为机器人绕 n圈 后要走的步数
 * 2、再根据 机器人绕n圈后要走的步数 判断此时机器人所在的方位(即格子的上下左右边线上)，由此可得机器人的坐标及方向
 * 3、坐标为(0,0)时进行特殊处理，如果机器人未移动过，则指向 东，否则指向 南，需要一个变量判定机器人是否移动过。
 *
 * @author liuhuaicai
 * @since 2024-11-06 20:31
 */
public class Robot {
    public static void main(String[] args) {
        Robot robot = new Robot(6, 3);
        robot.step(2);
        robot.step(2);
        robot.getPos();
        robot.getDir();
        robot.step(2);
        robot.step(1);
        robot.step(4);
        robot.getPos();
        robot.getDir();
    }
    int px;
    int py;
    String[] dir = new String[]{"West", "North", "East", "South"};
    int mod;
    int cur;

    boolean move;

    public Robot(int width, int height) {
        px = width - 1;
        py = height - 1;
        mod = 2 * (px + py);
    }

    public void step(int num) {
        move = true;
        cur += num;
        cur %= mod;
    }

    public int[] getPos() {
        int[] info = moveDir();
        return new int[]{info[0], info[1]};
    }

    public String getDir() {
        int[] info = moveDir();
        if (info[0] == 0 && info[1] == 0) {
            return move ? dir[3] : dir[2];
        }
        return dir[info[2]];
    }

    private int[] moveDir() {
        if (cur <= px) {
            return new int[]{cur, 0, 2};
        }
        if (cur <= px + py) {
            return new int[]{px, cur - px, 1};
        }
        if (cur <= 2 * px + py) {
            return new int[]{2 * px + py - cur, py, 0};
        }
        if (cur <= 2 * px + 2 * py) {
            return new int[]{0, 2 * px + 2 * py - cur, 3};
        }
        return new int[]{0, 0, 2};
    }
}
