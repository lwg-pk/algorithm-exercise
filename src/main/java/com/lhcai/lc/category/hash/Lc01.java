package com.lhcai.lc.category.hash;

import java.util.HashMap;

/**
 * 1. 两数之和
 *
 * @author liuhuaicai
 * @create 2024-07-08 20:50
 */
public class Lc01 {
    public int[] twoSum(int[] nums, int target) {
        // 目标值 - 数1 = 数2
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(target - nums[i])) {
                return new int[]{i, map.get(target - nums[i])};
            }
            map.put(nums[i],i);
        }
        return new int[0];
    }
}
