package com.lhcai.lc.category.hash;

import java.util.Arrays;

/**
 * 128. 最长连续序列
 *
 * @author liuhuaicai
 * @create 2024-07-29 20:32
 */
public class Lc128 {
    public static void main(String[] args) {
        Lc128 hot03 = new Lc128();
        hot03.longestConsecutive(new int[]{1,2,0,1});
    }

    public int longestConsecutive(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        int[] array = Arrays.stream(nums).distinct().sorted().toArray();
        int max = 0;
        int total = 1;
        for (int i = 1; i < array.length; i++) {
            if (array[i] == array[i - 1] + 1) {
                total += 1;
            } else {
                max = Math.max(max, total);
                total = 1;
            }
        }
        return Math.max(max, total);
    }
}
