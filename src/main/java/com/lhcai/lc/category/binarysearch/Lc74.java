package com.lhcai.lc.category.binarysearch;

/**
 * 74. 搜索二维矩阵
 *
 * @author liuhuaicai
 * @since 2024-11-20 20:39
 */
public class Lc74 {
    public boolean searchMatrix(int[][] matrix, int target) {
        // 二分法搜索
        for (int i = 0; i < matrix.length; i++) {
            int l = 0;
            int r = matrix[i].length - 1;
            while (l <= r) {
                int middle = l + (r - l) / 2;
                if (matrix[i][middle] == target) {
                    return true;
                } else if (matrix[i][middle] > target) {
                    r = middle - 1;
                } else {
                    l = middle + 1;
                }
            }
        }
        return false;
    }
}
