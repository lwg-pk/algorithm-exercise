package com.lhcai.lc.category.binarysearch;

/**
 * 35. 搜索插入位置
 *
 * @author liuhuaicai
 * @since 2024-11-20 20:26
 */
public class Lc35 {
    public static void main(String[] args) {
        Lc35 lc35 = new Lc35();
        System.out.println(lc35.searchInsert(new int[]{1, 2, 3, 5, 6, 8}, 7));
    }

    public int searchInsert(int[] nums, int target) {
        int n = nums.length;
        int l = 0;
        int r = n - 1;
        while (l <= r) {
            int middle = (l + r) / 2;
            if (nums[middle] == target) {
                return middle;
            } else if (nums[middle] < target) {
                l = middle + 1;
            } else {
                r = middle - 1;
            }
        }
        return l;
    }
}
