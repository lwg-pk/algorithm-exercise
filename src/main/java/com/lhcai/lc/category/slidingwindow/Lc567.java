package com.lhcai.lc.category.slidingwindow;

/**
 * 567. 字符串的排列
 * 找到字符串中所有字母异位词 的简化版本
 *
 * @author liuhuaicai
 * @since 2024-10-16 21:22
 */
public class Lc567 {
    public static void main(String[] args) {
        Lc567 lc567 = new Lc567();
        System.out.println(lc567.checkInclusion("ab", "eidbaooo"));
    }

    public boolean checkInclusion(String s1, String s2) {
        // 核心就是s2是否有一个子串，包含所有的字符
        int[] target = new int[26];
        for (int i = 0; i < s1.length(); i++) {
            target[s1.charAt(i) - 'a']++;
        }
        int left = 0;
        for (int right = 0; right < s2.length(); right++) {
            target[s2.charAt(right) - 'a']--;
            while (target[s2.charAt(right) - 'a'] < 0) {
                target[s2.charAt(left) -'a']++;
                left++;
            }
            if (right - left + 1 == s1.length()) {
                return true;
            }
        }
        return false;
    }
}
