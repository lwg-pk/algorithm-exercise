package com.lhcai.lc.category.slidingwindow;

import java.util.HashMap;
import java.util.Map;

/**
 * 3. 无重复字符的最长子串
 *
 * @author liuhuaicai
 * @since 2024-10-13 20:44
 */
public class Lc03 {
    public static void main(String[] args) {
        Lc03 lc03 = new Lc03();
        System.out.println(lc03.lengthOfLongestSubstring("abcabcbb"));
    }

    public int lengthOfLongestSubstring(String s) {
        int res = 0;
        Map<Character, Integer> map = new HashMap<>();
        for (int end = 0, start = 0; end < s.length(); end++) {
            char ch = s.charAt(end);
            // 存在则说明出现重复的了
            if (map.containsKey(ch)) {
                // 此时窗口向右移动到第一次出现重复的地方
                start = Math.max(start, map.get(ch));
            }
            // 窗口不断扩大
            res = Math.max(res, end - start + 1);
            // value 值为字符位置 +1，加 1 表示从字符位置后一个才开始不重复
            map.put(ch, end + 1);
        }
        return res;
    }
}
