package com.lhcai.lc.category.slidingwindow;


/**
 * 209. 长度最小的子数组
 *
 * @author liuhuaicai
 * @since 2024-12-30 22:44
 */
public class Lc209 {
    public static void main(String[] args) {
        Lc209 lc209 = new Lc209();
        System.out.println(lc209.minSubArrayLen(7, new int[]{2, 3, 1, 2, 4, 3}));
    }

    public int minSubArrayLen(int target, int[] nums) {
        int left = 0;
        int min = Integer.MAX_VALUE;
        int count = 0;
        for (int right = 0; right < nums.length; right++) {
            count += nums[right];
            // 窗口开始滑动，窗口可以变化
            while (count >= target && left <= right) {
                min = Math.min(min, right - left + 1);
                count -= nums[left];
                left++;
            }

        }
        return min == Integer.MAX_VALUE ? 0 : min;
    }
}
