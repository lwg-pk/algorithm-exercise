package com.lhcai.lc.category.slidingwindow;

/**
 * 242. 有效的字母异位词
 *
 * @author liuhuaicai
 * @since 2024-10-16 21:40
 */
public class Lc242 {
    public boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        int[] arr = new int[26];
        for (int i = 0; i < s.length(); i++) {
            arr[s.charAt(i) - 'a']++;
            arr[t.charAt(i) - 'a']--;
        }
        for (int i : arr) {
            if (i != 0) {
                return false;
            }
        }
        return true;
    }
}
