package com.lhcai.lc.category.tips;

import java.util.Arrays;

/**
 * 169. 多数元素
 *
 * @author liuhuaicai
 * @since 2025-01-05 20:45
 */
public class Lc169 {
    public static void main(String[] args) {
        Lc169 lc169 = new Lc169();
        System.out.println(lc169.majorityElement(new int[]{3, 3, 4}));
    }

    public int majorityElement(int[] nums) {
        Arrays.sort(nums);
        return nums[nums.length / 2];
    }

//    public int majorityElement(int[] nums) {
//        Arrays.sort(nums);
//        int res = 0;
//        int count = 0;
//        for (int num : nums) {
//            if (num == res) {
//                count++;
//            } else {
//                res = num;
//                count = 1;
//            }
//            if (count > nums.length / 2) {
//                break;
//            }
//
//        }
//        return res;
//    }
}
