package com.lhcai.lc.category.tips;

/**
 * 136. 只出现一次的数字
 *
 * @author liuhuaicai
 * @since 2025-01-05 20:11
 */
public class Lc136 {
    public static void main(String[] args) {
        Lc136 lc136 = new Lc136();
        System.out.println(lc136.singleNumber(new int[]{4, 1, 2, 1, 2}));
    }

    public int singleNumber(int[] nums) {
        // 异或
        int res = 0;
        for (int num : nums) {
            res ^= num;
        }
        return res;
    }

//    public int singleNumber(int[] nums) {
//        Arrays.sort(nums);
//        int l = 0;
//        int r = nums.length - 1;
//        while (l < r) {
//            int middle = (l + r + 1) >> 1;
//            if (nums[middle] == nums[middle + 2 * (middle % 2) - 1]) {
//                r = middle - 1;
//            } else {
//                l = middle;
//            }
//        }
//        return nums[l];
//    }

    // 暴力
//    public int singleNumber(int[] nums) {
//        Map<Integer, Integer> map = new HashMap<>();
//        for (int num : nums) {
//            if (map.containsKey(num)) {
//                map.remove(num);
//            } else {
//                map.put(num, num);
//            }
//        }
//        return map.values().stream().findFirst().orElse(-1);
//    }
}
