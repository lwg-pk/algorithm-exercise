package com.lhcai.lc.category.doublepointer;

/**
 * 283. 移动零
 *
 * @author liuhuaicai
 * @create 2024-07-29 21:06
 */
public class Lc283 {
    public static void main(String[] args) {
        moveZeroes(new int[]{1,0,1,1,1,1});
    }

    //    public static void moveZeroes(int[] nums) {
//        int left = 0;
//        int right = 1;
//        while (left <= right && left < nums.length && right < nums.length) {
//            if (nums[left] == 0) {
//                while (nums[right] == 0 && right < nums.length - 1) {
//                    right++;
//                }
//                exchange(nums, left, right);
//            }
//            if (right <= left) {
//                left++;
//                right++;
//            } else {
//                left++;
//            }
//        }
//    }
//
//
//    private static void exchange(int[] nums, int left, int right) {
//        int tmp = nums[left];
//        nums[left] = nums[right];
//        nums[right] = tmp;
//    }
    public static void moveZeroes(int[] nums) {
        int n = nums.length, left = 0, right = 0;
        while (right < n) {
            if (nums[right] != 0) {
                swap(nums, left, right);
                left++;
            }
            right++;
        }
    }

    public static void swap(int[] nums, int left, int right) {
        int temp = nums[left];
        nums[left] = nums[right];
        nums[right] = temp;
    }
}
