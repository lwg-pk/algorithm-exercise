package com.lhcai.lc.category.array;

/**
 * 189. 轮转数组
 * 也可以通过三次翻转实现
 *
 * @author liuhuaicai
 * @since 2024-11-18 20:01
 */
public class Lc189 {
    public static void main(String[] args) {
        Lc189 lc189 = new Lc189();
        lc189.rotate(new int[]{1, 2, 3, 4, 5, 6, 7}, 3);
    }

    public void rotate(int[] nums, int k) {
        int[] clone = nums.clone();
        int n = nums.length;
        int remainder = k % n;
        int count = 0;
        for (int i = n - remainder; i < n; i++) {
            nums[count] = clone[i];
            count++;
        }
        for (int i = 0; i < n - remainder; i++) {
            nums[count] = clone[i];
            count++;
        }
    }
}
