package com.lhcai.lc.category.prefixsum;

import java.util.Arrays;

/**
 * 724. 寻找数组的中心下标
 *
 * @author liuhuaicai
 * @since 2024-10-17 22:43
 */
public class Lc724 {
    public static void main(String[] args) {
        Lc724 lc724 = new Lc724();
        System.out.println(lc724.pivotIndex(new int[]{2, 1, -1}));
    }

    public int pivotIndex(int[] nums) {
        int total = Arrays.stream(nums).sum();
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            if (sum * 2 + nums[i] == total) {
                return i;
            }
            sum += nums[i];
        }
        return -1;
    }
}
