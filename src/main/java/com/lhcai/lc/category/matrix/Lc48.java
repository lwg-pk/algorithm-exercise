package com.lhcai.lc.category.matrix;

/**
 * 48. 旋转图像
 *
 * @author liuhuaicai
 * @since 2025-01-04 19:38
 */
public class Lc48 {
    public static void main(String[] args) {
        Lc48 lc48 = new Lc48();
        lc48.rotate(new int[][]{{5, 1, 9, 11}, {2, 4, 8, 10}, {13, 3, 6, 7}, {15, 14, 12, 16}});
    }

    public void rotate(int[][] matrix) {
        // 先选水平旋转，上下交换位置；再对角线旋转，对角线左右交换位置
//        matrix[row][col] = matrix[n - 1 - row][col];
//        matrix[row][col] = matrix[col][row];
//        matrix[col][n - 1 - row] = matrix[row][col];
        int n = matrix.length;
        // 水平旋转
        for (int i = 0; i < n / 2; i++) {
            for (int j = 0; j < n; j++) {
                int tmp = matrix[i][j];
                matrix[i][j] = matrix[n - 1 - i][j];
                matrix[n - 1 - i][j] = tmp;
            }
        }
        // 对角线旋转
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                int tmp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = tmp;
            }
        }
    }


//    public void rotate(int[][] matrix) {
//        // 使用额外的二维数组做辅助，一行行挪
//        int n = matrix.length;
//        int[][] arr = new int[n][n];
//        for (int i = 0; i < n; i++) {
//            for (int j = 0; j < n; j++) {
//                arr[j][n - 1 - i] = matrix[i][j];
//            }
//        }
//        for (int i = 0; i < n; i++) {
//            for (int j = 0; j < n; j++) {
//                matrix[i][j] = arr[i][j];
//            }
//        }
//        int row = 0;
//        int col = 0;
//        // matrix[col][n−row−1]=matrix[row][col]
//
//        // 第一个位置matrix[row][col]
//        matrix[col][n - 1 - row] = matrix[row][col];
//
//        // 第二个位置matrix[col][n - 1 - row]  row = col , col = n - 1 - row
//        matrix[n - 1 - row][n - 1 - col] = matrix[col][n - 1 - row];
//
//        // 第三个位置 matrix[n - 1 - row][n - 1 - col] row = n- 1 - row, col = n - 1 -col
//        matrix[n - 1 - col][row] = matrix[n - 1 - row][n - 1 - col];
//
//        // 第四个位置matrix[n - 1 - col][row] row = n -1 -col,col = row
//        matrix[row][col] = matrix[n - 1 - col][row];
//
//
//        int tmp = matrix[row][col];
//        matrix[row][col] = matrix[n - 1 - col][row];
//        matrix[n - 1 - col][row] = matrix[n - 1 - row][n - 1 - col];
//        matrix[n - 1 - row][n - 1 - col] = matrix[col][n - 1 - row];
//        matrix[col][n - 1 - row] = tmp;
//    }
}
