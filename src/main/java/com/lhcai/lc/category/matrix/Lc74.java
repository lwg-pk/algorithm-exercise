package com.lhcai.lc.category.matrix;

/**
 * 74. 搜索二维矩阵
 *
 * @author liuhuaicai
 * @since 2025-01-04 20:58
 */
public class Lc74 {
    public static void main(String[] args) {
        Lc74 lc74 = new Lc74();
        System.out.println(lc74.searchMatrix(new int[][]{{1}}, 1));
    }

    public boolean searchMatrix(int[][] matrix, int target) {
        for (int[] ints : matrix) {
            if (ints[ints.length - 1] < target) {
                continue;
            }
            if (ints[0] > target) {
                return false;
            }
            if (search(ints, target)) {
                return true;
            }
        }
        return false;
    }

    private boolean search(int[] ints, int target) {
        // 二分查找
        int left = 0;
        int right = ints.length - 1;
        while (left <= right) {
            int middle = (left + right) >> 1;
            if (ints[middle] == target) {
                return true;
            } else if (ints[middle] > target) {
                right = middle - 1;
            } else {
                left = middle + 1;
            }
        }
        return false;
    }
}
