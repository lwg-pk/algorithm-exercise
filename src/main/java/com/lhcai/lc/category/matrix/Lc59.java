package com.lhcai.lc.category.matrix;

import java.util.Arrays;

/**
 * 59. 螺旋矩阵 II
 *
 * @author liuhuaicai
 * @since 2025-01-04 21:22
 */
public class Lc59 {
    public static void main(String[] args) {
        Lc59 lc59 = new Lc59();
        System.out.println(Arrays.deepToString(lc59.generateMatrix(4)));
    }

    public int[][] generateMatrix(int n) {
        // 一个个处理，碰头转向，直到最后
        // 方向数组
        int[][] dir = new int[][]{{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
        int curIndex = 0;
        // 记录数组
        boolean[][] flag = new boolean[n][n];
        // 数组元素
        int count = 1;
        // 数组
        int[][] res = new int[n][n];
        // 数组索引
        int row = 0;
        int col = 0;
        for (int i = 0; i < n * n; i++) {
            res[row][col] = count;
            flag[row][col] = true;
            count++;
            // 判断
            int nextRow = row + dir[curIndex][0];
            int nextCol = col + dir[curIndex][1];
            if (nextRow < 0 || nextCol < 0 || nextRow >= n || nextCol >= n || flag[nextRow][nextCol]) {
                curIndex = (curIndex + 1) % 4;
            }
            row = row + dir[curIndex][0];
            col = col + dir[curIndex][1];
        }
        return res;
    }


    // 一行行处理，缩小内径
//    public int[][] generateMatrix(int n) {
//        int[][] res = new int[n][n];
//        int left = 0;
//        int right = n - 1;
//        int top = 0;
//        int bot = n - 1;
//        int count = 1;
//        while (left <= right && top <= bot) {
//            // 首行，到最右，顺
//            for (int i = left; i <= right; i++) {
//                res[top][i] = count;
//                count++;
//            }
//            // 尾列,到最下，顺
//            for (int i = top + 1; i <= bot; i++) {
//                res[i][right] = count;
//                count++;
//            }
//            if (left < right && top < bot) {
//                // 尾行，到最左，逆
//                for (int i = right - 1; i >= left; i--) {
//                    res[bot][i] = count;
//                    count++;
//                }
//                // 尾列，到最上，顺
//                for (int i = bot - 1; i >= top + 1; i--) {
//                    res[i][left] = count;
//                    count++;
//                }
//
//            }
//            left++;
//            right--;
//            top++;
//            bot--;
//        }
//        return res;
//    }
}
