package com.lhcai.lc.category.matrix;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 885. 螺旋矩阵 III
 *
 * @author liuhuaicai
 * @since 2025-01-04 22:05
 */
public class Lc885 {
    public static void main(String[] args) {
        Lc885 lc885 = new Lc885();
        System.out.println(Arrays.deepToString(lc885.spiralMatrixIII(5, 6, 1, 4)));
    }

    public int[][] spiralMatrixIII(int rows, int cols, int rStart, int cStart) {
        //  1 1 2 2 3 3 4 4 5 5
        List<int[]> res = new ArrayList<>();
        // 方向数组
        int[][] dir = new int[][]{{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
        // 当前方位
        int curIndex = 0;
        int step = 0;
        res.add(new int[]{rStart, cStart});
        while (res.size() < rows * cols) {
            // 从方向0开始，每转向两次步长加一
            if (curIndex % 2 == 0) {
                step++;
            }
            // 此次步长内有符合的则加入
            for (int i = 0; i < step; i++) {
                rStart += dir[curIndex][0];
                cStart += dir[curIndex][1];
                if (rStart >= 0 && cStart >= 0 && rStart < rows && cStart < cols) {
                    res.add(new int[]{rStart, cStart});
                }
            }
            // 转向
            curIndex = (curIndex + 1) % 4;
        }
        return res.toArray(new int[0][]);
    }
}
