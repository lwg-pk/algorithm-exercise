package com.lhcai.nk;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author lhcstart
 * @create 2023-02-26 15:39
 */

/**
 * 入职后，导师会请你吃饭，你选择了火锅。
 * 火锅里会在不同时间下很多菜。
 * 不同食材要煮不同的时间，才能变得刚好合适。
 * 你希望吃到最多的刚好合适的菜，但你的手速不够快，用m代表手速，每次下手捞菜后至少要过m秒才能再捞（每次只能捞一个）。
 * 那么用最合理的策略，最多能吃到多少刚好合适的菜？
 * 输入描述
 * 第一行两个整数n，m，其中n代表往锅里下的菜的个数，m代表手速。
 * 接下来有n行，每行有两个数x，y代表第x秒下的菜过y秒才能变得刚好合适。
 * （1 < n, m < 1000）（1 < x, y < 1000）
 * 输出描述
 * 输出一个整数代表用最合理的策略，最多能吃到刚好合适的菜的数量。
 */
public class ChafingDish {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {

            int n = sc.nextInt();//菜的个数
            int m = sc.nextInt();//手速

            int[] arrTime = new int[n];//菜能吃的时间
            for (int i = 0; i < n; i++) {
                int x = sc.nextInt();
                int y = sc.nextInt();
                arrTime[i] = x + y;
            }
            Arrays.sort(arrTime);//排序

            int[] resCount = new int[n];//存放能吃到的菜，能为1，不能为0
            int pre = 0;
            resCount[0] = 1;//绝对能吃到1个菜


            for (int i = 1; i < arrTime.length; i++) {

                if (arrTime[i] >= arrTime[pre] + m) {//下一个菜的吃菜时间大于上一个菜的时间+手速 ，能吃到
                    resCount[i] = 1;//设为能吃
                    pre = i;//下次吃菜计算从i开始
                }
            }

            int res = 0;
            for (int i : resCount) {
                res += i;
            }
            System.out.println(res);

        }
    }
}
