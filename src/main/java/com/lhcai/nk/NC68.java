package com.lhcai.nk;


/**
 * @author lhcstart
 * @create 2023-02-22 21:02
 */

/**
 * 描述
 * 一只青蛙一次可以跳上1级台阶，也可以跳上2级。求该青蛙跳上一个 n 级的台阶总共有多少种跳法（先后次序不同算不同的结果）。
 * 数据范围： 1≤n≤40
 * 要求：时间复杂度： O(n) ，空间复杂度：  O(1)
 */
public class NC68 {
    public int jumpFloor(int target) {
        if (target == 1) {
            return 1;
        }
        if (target == 2) {
            return 2;
        }

        return jumpFloor(target - 1) + jumpFloor(target - 2);
    }

    public int jumpFloor1(int target) {
        //从0开始，第0项是0，第一项是1
        if(target <= 1){
            return 1;
        }
        int res = 0;
        int a = 0;
        int b = 1;
        //因n=2时也为1，初始化的时候把a=0，b=1
        for(int i = 2; i <= target; i++){
            //第三项开始是前两项的和,然后保留最新的两项，更新数据相加
            res = (a + b);
            a = b;
            b = res;
        }
        return res;
    }

}

 class Solution68 {
    //设置全局变量，因为实际问题中没有0，则可用0作初始标识值
    private int[] dp = new int[50];
    public int F(int n){
        if(n <= 1)
            return 1;
        //若是dp中有值则不需要重新递归加一次
        if(dp[n] != 0)
            return dp[n];
        //若是dp中没有值则需要重新递归加一次
        return dp[n] = F(n - 1) + F(n - 2);
    }
    public int jumpFloor(int target) {
        return F(target);
    }
}

class Solution68_4 {
    public int jumpFloor(int target) {
        //从0开始，第0项是1，第一项是1
        if (target <= 1)
            return 1;
        int[] temp = new int[target + 1];
        //初始化
        temp[0] = 1;
        temp[1] = 1;
        //遍历相加
        for (int i = 2; i <= target; i++)
            temp[i] = temp[i - 1] + temp[i - 2];
        return temp[target];
    }
}
