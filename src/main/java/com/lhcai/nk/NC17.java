package com.lhcai.nk;

/**
 * @author lhcstart
 * @create 2023-02-24 21:34
 */

/**
 * 最长回文子串问题
 */
public class NC17 {
    public static void main(String[] args) {
        NC17 nc = new NC17();
        int dp = nc.dp("ababc");
        System.out.println(dp);
    }

    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * @param A string字符串
     * @return int整型
     */
    public int getLongestPalindrome(String A) {
        // write code here
        int dp = dp(A);
        return dp;

    }

    public int dp(String s) {
        int len = s.length();
        boolean[][] dp = new boolean[len][len];

        for (int i = 0; i < len - 1; i++) {
            dp[i][i] = true;
        }
        int res = 1;
        for (int r = 1; r < len; r++) {
            for (int l = 0; l < r; l++) {
                if (s.charAt(l) == s.charAt(r) && (dp[l + 1][r - 1] || r - l < 2)) {
                    dp[l][r] = true;
                    res = Math.max(res, r - l + 1);
                }
            }
        }
        return res;
    }
}
