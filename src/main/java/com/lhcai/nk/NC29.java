package com.lhcai.nk;

/**
 * @author lhcstart
 * @create 2023-03-02 19:51
 */

/**
 *二维数组中的查找
 */
public class NC29 {
    public boolean Find(int target, int [][] array) {
        int rows = array.length;
        if (rows == 0) {
            return false;
        }
        int cols = array[0].length;
        if (cols == 0) {
            return false;
        }


        int row = rows - 1;
        int col = 0;

        while (row >= 0 && col < cols) {
            if (array[row][col] > target) {
                row--;
            } else if (array[row][col] < target) {
                col++;
            } else {
                return true;
            }
        }
        return false;
    }
}
