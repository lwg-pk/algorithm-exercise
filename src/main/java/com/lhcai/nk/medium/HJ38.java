package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-18 19:51
 */

import java.util.Scanner;

/**
 * 描述
 * 假设一个球从任意高度自由落下，每次落地后反跳回原高度的一半; 再落下, 求它在第5次落地时，共经历多少米?第5次反弹多高？
 * <p>
 * <p>
 * 数据范围：输入的小球初始高度满足 1≤n≤1000  ，且保证是一个整数
 * <p>
 * 输入描述：
 * 输入起始高度，int型
 * <p>
 * 输出描述：
 * 分别输出第5次落地时，共经过多少米以及第5次反弹多高。
 * 注意：你可以认为你输出保留六位或以上小数的结果可以通过此题。
 */
public class HJ38 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            double len = sc.nextDouble();
            double temp = len / 2;
            for (int i = 1; i < 5; i++) {
                len += temp * 2;
                temp = temp / 2;

            }
            System.out.println(len);
            System.out.println(temp);
        }
    }
}
