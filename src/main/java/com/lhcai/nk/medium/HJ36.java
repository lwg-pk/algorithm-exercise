package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-17 21:52
 */
import java.util.*;
/**
 * 字符串加密
 */
public class HJ36 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String key = sc.next();
            String ps = sc.next();
            //将密钥去重后放入list
            ArrayList<Character> list = new ArrayList();
            for (int i = 0; i < key.length(); i++) {
                if (!list.contains(key.charAt(i))) {
                    list.add(key.charAt(i));
                }
            }
            //添加a-z,获取密码表
            for (int i = 0; i < 26; i++) {
                char c = (char) (i + 97);
                if (!list.contains(c)) {
                    list.add(c);
                }
            }
//**********************解法一
//            String result =    "";
//            for(int i=0;i<ps.length();i++){
//                if(ps.charAt(i)>=97 && ps.charAt(i)<=122){
//                    result += list.get(ps.charAt(i)-97);
//                }else {
//                    result += Character.toUpperCase(list.get(ps.toLowerCase().charAt(i)-97));
//                }
//            }

//**********************解法二
            //创建map保存key(正常字母)，value(密钥)
            Map<Character, Character> map = new HashMap<>();
            int begin = 97;
            for (int i = 0; i < list.size(); i++) {
                map.put((char) (begin + i), list.get(i));
            }
            //加密数据
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < ps.length(); i++) {
                char c = ps.charAt(i);
                Character character = map.get(c);
                if (c >= 97 && c <= 122) {
                    result.append(character);
                } else {
                    result.append(Character.toUpperCase(character));
                }
            }
            System.out.println(result.toString());
        }
    }
}

