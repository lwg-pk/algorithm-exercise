package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-20 10:51
 */

import java.util.Scanner;


/**
 * 字符串通配符
 */
public class HJ71 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String pattern = sc.next();
            String str = sc.next();
            String s = str.toLowerCase();
            String p = pattern.toLowerCase();

            System.out.println(solution(s, p));
        }
    }

    public static boolean solution(String s, String p) {
        int m = s.length();
        int n = p.length();
        boolean[][] dp = new boolean[m + 1][n + 1];

        //边界条件
        dp[0][0] = true;
        for (int i = 1; i <= n; i++) {
            if (p.charAt(i-1) == '*') {
                //字符串为空，通配符*：匹配0个或以上的字符
                dp[0][i] = true;
            } else {
                break;
            }
        }

        for (int i = 1; i <= m; i++) {
            //匹配
            for (int j = 1; j <= n; j++) {
                //获取字符串
                char ch = s.charAt(i-1);

                if (p.charAt(j - 1) == '*' && (Character.isDigit(ch) || Character.isLetter(ch))) {
                    dp[i][j] = dp[i][j - 1] | dp[i - 1][j] | dp[i - 1][j - 1];
                } else if (p.charAt(j - 1) == '?' && (Character.isDigit(ch) || Character.isLetter(ch))) {
                    dp[i][j] = dp[i - 1][j - 1];
                } else if (p.charAt(j - 1) == s.charAt(i - 1)) {
                    dp[i][j] = dp[i - 1][j - 1];
                }

            }
        }
        return dp[m][n];
    }
}

//*****************正则解法**********
//    public static void main(String[] args) throws IOException {
//        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
//        String value;
//        while ((value = bf.readLine()) != null) {
//            String target = bf.readLine();

//            value = value.toLowerCase(Locale.ROOT);
//            target = target.toLowerCase(Locale.ROOT);

//            String regx = value.replaceAll("\\*{2,}","\\*");
//            regx = regx.replaceAll("\\?","[0-9a-z]{1}");
//            regx = regx.replaceAll("\\*","[0-9a-z]{0,}");
//            System.out.println(target.matches(regx));
//        }
//    }
