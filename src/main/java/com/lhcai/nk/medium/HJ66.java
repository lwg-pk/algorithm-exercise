package com.lhcai.nk.medium;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * @author lhcstart
 * @create 2023-02-21 8:43
 */

/**
 * 配置文件恢复
 */
public class HJ66 {
    public static void main(String[] args) {

        HashMap<String, String> map = new HashMap<>();
        map.put("reset", "reset what");
        map.put("reset board", "board fault");
        map.put("board add", "where to add");
        map.put("board delete", "no board at all");
        map.put("reboot backplane", "impossible");
        map.put("backplane abort", "install first");
        map.put("noCommand", "unknown command");

        HashSet<String[]> set = new HashSet<>();
        for (String s : map.keySet()) {
            set.add(s.split(" "));
        }

        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String str = sc.nextLine();
            String[] arr = str.split(" ");
            int count = 0;
            String res = "noCommand";//设置初始key，则所有不匹配的都会输出为unknown command；

            for (String[] s : set) {

                if (arr.length == 1) {
                    if (s.length == 2) {
                        continue;
                    } else if (s[0].startsWith(arr[0])) {
                        res = s[0];
                    }
                }

                if (arr.length == 2) {
                    if (s.length == 1) {
                        continue;
                    } else if (s[0].startsWith(arr[0]) && s[1].startsWith(arr[1])) {
                        res = s[0] + " " + s[1];
                        count++;
                    }
                }
            }

            System.out.println(count>1?"unknown command":map.get(res));
        }
    }
}
