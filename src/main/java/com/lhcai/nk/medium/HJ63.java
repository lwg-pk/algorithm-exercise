package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-18 21:47
 */

import java.util.Scanner;

/**
 * DNA序列
 */
public class HJ63 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String s = sc.next();
            int n = sc.nextInt();

            int maxNum = 0;
            String res = "";
            int index = 0;

            for (int i = 0; i < s.length() - n + 1; i++) {
                int gcNum = 0;
                for (int j = i; j < i + n; j++) {
                    if (s.charAt(j) == 'C' || s.charAt(j) == 'G') {
                        gcNum++;
                    }
                }
                if (gcNum > maxNum) {
                    index = i;
                    maxNum = gcNum;
                    if (gcNum == n) {
                        System.out.println(s.substring(index, index + n));
                        return;
                    }
                }
            }
            System.out.println(s.substring(index, index + n));

        }
    }


    //*********正则表达式********
    public static String solution(String s, int n) {
        double max = 0.0;
        String res = "";
        for (int i = 0; i < s.length() - n + 1; i++) {
            String str = s.substring(i, i + n);
            String s1 = str.replaceAll("[^CG]", "");
            double num = (double) s1.length() / (double) n;
            if (num > max) {
                max = num;
                res = str;
            }
        }
        return res;
    }
}
