package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-18 20:58
 */

import java.util.Scanner;

/**
 * 合法IP
 */
public class HJ90 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String s = sc.nextLine();

            if (s.contains(" ")) {
                System.out.println("NO");
                return;
            }
            //长度不等于4
            String[] split = s.split("\\.");
            if (split.length != 4) {
                System.out.println("NO");
                return;
            }
            //遍历每一段
            for (int i = 0; i < split.length; i++) {
                String str = split[i];
                //某段为空
//                if (str.equals(" ") ||str.equals("")||str==null){
//                    System.out.println("NO");
//                    return;
//                }
                int n = str.length();
                //某段长度不符合
                if (n == 0 || n > 3) {
                    System.out.println("NO");
                    return;
                }
                //某段元素里存在非数字或者 除0以外，以0开头
                for (int j = 0; j < n; j++) {
                    if (!Character.isDigit(str.charAt(j)) || (n >= 2 && str.charAt(0) == '0')) {
                        System.out.println("NO");
                        return;
                    }
                }
                //每段对应的数大于255，也是非法的
                if (Integer.parseInt(str) < 0 || Integer.parseInt(str) > 255) {
                    System.out.println("NO");
                    return;
                }
            }
            System.out.println("YES");

        }
    }
}
