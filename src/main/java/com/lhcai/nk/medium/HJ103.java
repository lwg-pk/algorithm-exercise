package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-20 17:25
 */

import java.util.Arrays;
import java.util.Scanner;

/**
 * 描述
 * Redraiment是走梅花桩的高手。Redraiment可以选择任意一个起点，从前到后，但只能从低处往高处的桩子走。他希望走的步数最多，你能替Redraiment研究他最多走的步数吗？
 * 数据范围：每组数据长度满足 1≤n≤200  ， 数据大小满足  1≤val≤350
 * 输入描述：
 * 数据共2行，第1行先输入数组的个数，第2行再输入梅花桩的高度
 * 输出描述：
 * 输出一个结果
 */
public class HJ103 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int n = Integer.parseInt(sc.nextLine());
            String s = sc.nextLine();
            String[] str = s.split(" ");
            int[] nums = new int[n];

            //将string转换成int数组
            for (int i = 0; i < n; i++) {
                nums[i] = Integer.parseInt(str[i]);
            }
            //创建动态数组，初始化为1
            int[] dp = new int[n + 1];
            Arrays.fill(dp, 1);

            int max = 1;
            for (int i = 1; i < nums.length; i++) {
                for (int j = 0; j < i; j++) {
                    if (nums[j] < nums[i]) {
                        dp[i] = Math.max(dp[i], dp[j] + 1);
                    }
                    max = Math.max(dp[i],max);
                }
            }
            System.out.println(max);
        }
    }
}
