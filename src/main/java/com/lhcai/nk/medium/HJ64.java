package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-21 15:41
 */

import java.util.Scanner;

/**
 * MP3光标位置
 */

/**
 * 假设每屏只能显示4首歌曲，光标初始的位置为第1首歌。
 * 歌曲总数<=4的时候，不需要翻页，只是挪动光标位置。
 * 光标在第一首歌曲上时，按Up键光标挪到最后一首歌曲；
 * 光标在最后一首歌曲时，按Down键光标挪到第一首歌曲。
 * 其他情况下用户按Up键，光标挪到上一首歌曲；
 * 用户按Down键，光标挪到下一首歌曲。
 */
public class HJ64 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int n = sc.nextInt();
            String s = sc.next();

            int pagesize = 4;//页面数量
            int current = 1;//当前歌曲光标
            int pageindex = 1;//当前歌曲在页面中的光标，范围：1-4

            //歌数量小于4
            if (n < 4) {
                pagesize = n;
            }

            for (int i = 0; i < s.length(); i++) {

                //命令为U
                if (s.charAt(i) == 'U') {
                    //当前歌曲为第一首，特殊操作
                    if (current == 1) {
                        //将当前歌曲光标挪到最后一首
                        current = n;
                        //将当前页面光标挪到最后一个
                        pageindex = pagesize;
                    } else {
                        //其他情况，光标上移
                        current--;
                        //只要pageindex不是1，光标上移
                        //pageindex为1是上移，光标还在原来的位置
                        if (pageindex != 1) {
                            pageindex--;
                        }
                    }
                } else {
                    if (current == n) {
                        current = 1;
                        pageindex = 1;
                    } else {
                        current++;
                        if (pageindex != pagesize) {
                            pageindex++;
                        }
                    }
                }
            }
            //判断当前页面歌曲前后的歌曲数
            int next = pagesize - pageindex;
            int pre = pageindex - 1;

            //输出当前歌曲列表
            String res = "";
            for (int i = pre; i > 0; i--) {
                res += (current - i) + " ";
            }
            for (int j = 0; j <= next; j++) {
                res += (current + j) + " ";
            }
            System.out.println(res.substring(0,res.length()-1));
            System.out.println(current);

        }
    }
}
