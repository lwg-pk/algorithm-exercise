package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-17 15:27
 */

import java.util.Scanner;

/**
 * 描述
 * 对输入的字符串进行加解密，并输出。
 * 加密方法为：
 * 当内容是英文字母时则用该英文字母的后一个字母替换，同时字母变换大小写,如字母a时则替换为B；字母Z时则替换为a；
 * 当内容是数字时则把该数字加1，如0替换1，1替换2，9替换0；
 * 其他字符不做变化。
 * 解密方法为加密的逆过程。
 * 数据范围：输入的两个字符串长度满足1≤n≤1000  ，保证输入的字符串都是只由大小写字母或者数字组成
 * 输入描述：
 * 第一行输入一串要加密的密码
 * 第二行输入一串加过密的密码
 * 输出描述：
 * 第一行输出加密后的字符
 * 第二行输出解密后的字符
 */
public class HJ29 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String str = sc.nextLine();
            String str1 = sc.nextLine();
            System.out.println(aP(str));
            System.out.println(dP(str1));

        }
    }

    public static String aP(String s) {
        char[] chars = s.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < chars.length; i++) {
            char ch = chars[i];
            if (Character.isUpperCase(ch)) {
                if (ch == 'Z'){
                    sb.append('a');
                }else {
                    char c = Character.toLowerCase((char) (ch + 1));
                    sb.append(c);
                }
                continue;
            }else if (Character.isLowerCase(ch)) {
                if (ch == 'z'){
                    sb.append('A');
                }else {
                    char c = Character.toUpperCase((char) (ch + 1));
                    sb.append(c);
                }
                continue;
            }else if (Character.isDigit(ch)){
                if (ch == '9'){
                    sb.append('0');
                }else {
                    sb.append((char) (ch+1));
                }
            }else {
                sb.append(ch);
            }
        }
        return  sb.toString();
    }

    public static String dP(String s) {
        char[] chars = s.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < chars.length; i++) {
            char ch = chars[i];
            if (Character.isUpperCase(ch)) {
                if (ch == 'A'){
                    sb.append('z');
                }else {
                    char c = Character.toLowerCase((char) (ch -1));
                    sb.append(c);
                }
                continue;
            }else if (Character.isLowerCase(ch)) {
                if (ch == 'a'){
                    sb.append('Z');
                }else {
                    char c = Character.toUpperCase((char) (ch - 1));
                    sb.append(c);
                }
                continue;
            }else if (Character.isDigit(ch)){
                if (ch == '0'){
                    sb.append('9');
                }else {
                    sb.append((char) (ch-1));
                }
            }else {
                sb.append(ch);
            }
        }
        return  sb.toString();
    }

}
