package com.lhcai.nk.medium;
import java.util.Scanner;
/**
 * @author lhcstart
 * @create 2023-02-20 16:24
 */
/**
 * 描述
 * 输入一个字符串，返回其最长的数字子串，以及其长度。若有多个最长的数字子串，则将它们全部输出（按原字符串的相对位置）
 * 本题含有多组样例输入。
 *
 * 数据范围：字符串长度 1≤n≤200  ， 保证每组输入都至少含有一个数字
 * 输入描述：
 * 输入一个字符串。1<=len(字符串)<=200
 * 输出描述：
 * 输出字符串中最长的数字字符串和它的长度，中间用逗号间隔。如果有相同长度的串，则要一块儿输出（中间不要输出空格）。
 */
public class HJ92 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String s = sc.next();
            int[] dp = new int[s.length() + 1];
            dp[0] = 0;
            int maxlen = 0;
            for (int i = 1; i <= s.length(); i++) {
                if (Character.isDigit(s.charAt(i - 1))) {
                    dp[i] = dp[i - 1] + 1;
                }

                if (dp[i] > maxlen) {
                    maxlen = dp[i];
                }
            }
            for (int i = 1; i <= s.length(); i++) {
                if (dp[i] == maxlen){
                    System.out.print(s.substring(i-maxlen,i));
                }
            }
            System.out.println("," + maxlen);
        }
    }
}
//**********正则表达式******
//public class HJ92 {
//    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        while (sc.hasNext()) {
//            String s = sc.next();
//
//            String s1 = s.replaceAll("[^0-9]", " ");
//            String[] s2 = s1.split(" ");
//
//            int maxlen = 0;
//            for (int i = 0; i < s2.length; i++) {
//                if (s2[i].length() > maxlen) {
//                    maxlen = s2[i].length();
//                }
//            }
//
//            for (String s3 : s2) {
//                if (s3.length() == maxlen) {
//                    System.out.print(s3);
//                }
//            }
//            System.out.println("," + maxlen);
//
//        }
//    }
//}
