package com.lhcai.nk.medium;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author lhcstart
 * @create 2023-02-20 22:08
 */
/**
 * 描述
 * 给出一个字符串，该字符串仅由小写字母组成，定义这个字符串的“漂亮度”是其所有字母“漂亮度”的总和。
 * 每个字母都有一个“漂亮度”，范围在1到26之间。没有任何两个不同字母拥有相同的“漂亮度”。字母忽略大小写。
 * 给出多个字符串，计算每个字符串最大可能的“漂亮度”。
 * 本题含有多组数据。
 * 数据范围：输入的名字长度满足1≤n≤10000
 * 输入描述：
 * 第一行一个整数N，接下来N行每行一个字符串
 * 输出描述：
 * 每个字符串可能的最大漂亮程度
 */
public class HJ45 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int n = Integer.parseInt(sc.nextLine());
            for (int i = 0; i < n; i++) {
                String s = sc.nextLine();
                int[] letter = new int[128];
                for (int j = 0; j < s.length(); j++) {
                    letter[s.charAt(j)]++;
                }

                Arrays.sort(letter);

                int mul = 26;
                int nums = 0;
                for (int j = letter.length - 1; j >= 0; j--) {
                    nums += letter[j] * mul;
                    mul--;
                }
                System.out.println(nums);
            }
        }
    }
}
