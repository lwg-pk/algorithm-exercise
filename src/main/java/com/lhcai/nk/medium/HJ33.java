package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-17 17:44
 */


import java.util.Scanner;

/**
 * 整数与IP地址间的转换
 */
public class HJ33 {
    public final int N = 4;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        HJ33 solution = new HJ33();
        while (sc.hasNext()) {
            String res = solution.convert(sc.next());
            System.out.println(res);
        }
    }

    public String convert(String str) {
        // ipv4 -> int
        if (str.contains(".")) {
            long nums = 0;
            String[] split = str.split("\\.");
            for (int i = 0; i < 4; i++) {
                nums = nums * 256 + Long.parseLong(split[i]);
            }
            return String.valueOf(nums);
        }  // int -> ipv4
        else {
            long ipv4 = Long.parseLong(str);
            String nums = "";
            for (int i = 0; i < 4; i++) {
                nums = ipv4 % 256 + "." + nums;
                ipv4 /= 256;
            }
            return nums.substring(0, nums.length() - 1);
        }
    }
}

//********暴力循环法****使用long避免数组越界

//public class HJ33 {
//    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        while (sc.hasNext()) {
//            String s = sc.next();
//            if(s.contains(".")){
//                System.out.println(toMath(s));
//            }else{
//                System.out.println(toIP(Long.parseLong(s)));
//            }
//
//        }
//    }
//
//    public static long toMath(String s) {
//        String[] split = s.split("\\.");
//        StringBuilder sb = new StringBuilder();
//        for (int i = 0; i < split.length; i++) {
//            int va = Integer.parseInt(split[i]);//循环拆分
//            String str = Integer.toBinaryString(va);//转换成二进制
//            while (str.length() < 8) {//拼接补零
//                str = "0" + str;
//            }
//            sb.append(str);
//        }
//        return Long.parseLong(sb.toString(), 2);//使用long类型，避免int类型数组空间不够，发生角标越界异常
//    }
//
//
//    public static String toIP(long num) {
//        String nums = Long.toBinaryString(num);
//        StringBuilder sb = new StringBuilder();
//        while (nums.length() < 32) {//拼接补零
//            nums = "0" + nums;
//        }
//        String[] res = new String[4];
//        for (int i = 0; i < 4; i++) {
//            String s = nums.substring(i * 8, i * 8 + 8);
//            String s1 = Integer.toString(Integer.parseInt(s, 2));
//            res[i] = s1;
//        }
//        return String.join(".", res);
//
//    }
//
//}
