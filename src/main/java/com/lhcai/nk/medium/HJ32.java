package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-17 15:56
 */



import java.io.IOException;
import java.util.Scanner;


/**
 * 描述
 * Catcher是MCA国的情报员，他工作时发现敌国会用一些对称的密码进行通信，
 * 比如像这些ABBA，ABA，A，123321，但是他们有时会在开始或结束时加入一些无关的字符以防止别国破解。
 * 比如进行下列变化 ABBA->12ABBA,ABA->ABAKK,123321->51233214　。
 * 因为截获的串太长了，而且存在多种可能的情况（abaaab可看作是aba,或baaab的加密形式），
 * Cathcer的工作量实在是太大了，他只能向电脑高手求助，你能帮Catcher找出最长的有效密码串吗？
 * 数据范围：字符串长度满足1≤n≤2500
 * 输入描述：
 * 输入一个字符串（字符串的长度不超过2500）
 * 输出描述：
 * 返回有效密码串的最大长度
 */
public class HJ32 {
    public static void main(String[] args) throws IOException {
        //*************动态规划法**************
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String s = sc.nextLine();
            System.out.println(solution(s));
        }
    }

    public static int solution(String s) {
        int len = s.length();
        boolean[][] dp = new boolean[len][len];
        //初始化数组对角线元素即其自身为回文 ，true
        for (int i = 0; i < len - 1; i++) {
            dp[i][i] = true;
        }
        //假如s.charAt(l)==s.charAt(r) 同时dp[l+1][r-1]=true即为回文子串，则 dp[l][r]=true为回文子串
        int res = 1;
        for (int r = 1; r < len; r++) {
            for (int l = 0; l < r; l++) {
                //r-l小于2即ABA或者AA，中间只有一个元素或者没有元素
                if(s.charAt(l) == s.charAt(r) && (dp[l + 1][r - 1] || r - l < 2)) {
                    dp[l][r] = true;
                    res = Math.max(res, r - l + 1);
                }
            }
        }
        return  res;
    }
}
//*******************中心扩散法**********
/*
*
 * public class HJ32 {
 * public static void main(String[] args) throws IOException {
 * Scanner sc = new Scanner(System.in);
 * while (sc.hasNext()) {
 * String s = sc.nextLine();
 * System.out.println(solution(s));
 * }
 * }
 * public static int solution(String s) {
 * int res = 0;
 * for (int i = 0; i < s.length(); i++) {
 * int len1 = longres(s, i, i);
 * int len2 = longres(s, i, i + 1);
 * res = Math.max(res, len1 > len2 ? len1 : len2);
 * }
 * return res;
 * }
 * <p>
 * public static int longres(String s, int l, int r) {
 * int n = s.length();
 * while (l >= 0 && r < n && s.charAt(l) == s.charAt(r)) {
 * l--;
 * r++;
 * }
 * return r - l - 1;
 * }
 * }
 * <p>
 *
 //*****************双重for循环遍历*************超时
/**
 * public class HJ32 {
 * public static void main(String[] args) {
 * Scanner sc =new Scanner(System.in);
 * while (sc.hasNext()){
 * String s = sc.nextLine();
 * int n = s.length();
 * int max = Integer.MIN_VALUE;
 * for (int i = 0; i < n; i++) {
 * for (int j = n; j > i; j--) {
 * String s1 = s.substring(i, j);
 * if (isCode(s1)){
 * max = Math.max(max, j - i);
 * }
 * }
 * }
 * System.out.println(max);
 * }
 * }
 * <p>
 * <p>
 * public static boolean isCode(String s){
 * StringBuilder sb = new StringBuilder(s);
 * String str = sb.reverse().toString();
 * return s.equals(str);
 * }
 * }
 */
