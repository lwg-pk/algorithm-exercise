package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-18 20:09
 */


import java.util.Scanner;
import java.util.Stack;


/**
 * 描述
 * 输入一个表达式（用字符串表示），求这个表达式的值。
 * 保证字符串中的有效字符包括[‘0’-‘9’],‘+’,‘-’, ‘*’,‘/’ ,‘(’， ‘)’,‘[’, ‘]’,‘{’ ,‘}’。且表达式一定合法。
 * 数据范围：表达式计算结果和过程中满足∣val∣≤1000  ，字符串长度满足  1≤n≤1000
 * 输入描述：
 * 输入一个算术表达式
 * 输出描述：
 * 得到计算结果
 */
public class HJ50 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String str = sc.next();
            str = str.replace("{", "(");
            str = str.replace("[", "(");
            str = str.replace("}", ")");
            str = str.replace("]", ")");
            int solution = solution(str);
            System.out.println(solution);
        }

    }

    public static int solution(String str) {

        Stack<Integer> stack = new Stack();
        int n = str.length();
        char[] chars = str.toCharArray();
        int nums = 0;
        char sign = '+';

        for (int i = 0; i < n; i++) {

            if (chars[i] == ' ') {
                continue;
            }
            //数字，拼数字

            if (Character.isDigit(chars[i])) {
                nums = nums * 10 + chars[i] - '0';

            }

            //是括号，迭代计算括号
            if (chars[i] == '(') {
                int count = 1;
                int j = i + 1;
                while (count > 0) {
                    if (chars[j] == '(') {
                        count++;
                    } else if (chars[j] == ')') {
                        count--;
                    }
                    j++;
                }
                nums = solution(str.substring(i + 1, j - 1));
                i = j - 1;
            }
            //是符号，弹出数字计算
            if (!Character.isDigit(chars[i]) || i ==n-1){
                if (sign == '+') {
                    stack.push(nums);
                } else if (sign == '-') {
                    stack.push(-1 * nums);
                } else if (sign == '*') {
                    stack.push(stack.pop() * nums);
                } else if (sign == '/') {
                    stack.push(stack.pop() / nums);
                }
                //更新符号
                sign = chars[i];
                //更新数字
                nums = 0;
            }
        }
        //遍历栈中数据相加
        int ans = 0;
        while (!stack.isEmpty()) {
            ans += stack.pop();
        }
        return ans;
    }
}
