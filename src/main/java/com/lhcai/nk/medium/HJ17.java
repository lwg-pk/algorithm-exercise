package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-11 19:31
 */


import java.util.Scanner;

/**
 *
 */
public class HJ17 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] str = sc.nextLine().split(";");
        int res_x = 0;
        int res_y = 0;
        for (String s : str) {
            if (!s.matches("[ADSW][0-9]{1,2}")) {
                continue;
            }
            Integer v = Integer.valueOf(s.substring(1));
            switch (s.charAt(0)) {
                case 'A':
                    res_x -= v;
                    break;
                case 'D':
                    res_x += v;
                    break;
                case 'W':
                    res_y += v;
                    break;
                case 'S':
                    res_y -= v;
                    break;
            }


        }
        System.out.println(res_x +","+res_y);

    }

}

