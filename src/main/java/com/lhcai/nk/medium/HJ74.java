package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-20 12:05
 */

import java.util.ArrayList;
import java.util.Scanner;

/**
 * 参数解析
 */
public class HJ74 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String s = sc.nextLine();
            char[] chars = s.toCharArray();

            String str = "";
            ArrayList<String> list = new ArrayList<>();
            //设置标识旗，标识是否进入""范围
            boolean flag = false;
            for (int i = 0, j = i; i < chars.length; i++) {
                char ch = chars[i];
                //第一次遇到",改变旗子的状态
                if (ch == '"') {
                    flag = flag ? false : true;
                    continue;
                }
                //标识旗为false时，不在""范围内，遇到"空格"，加入到list并清空字符串
                if (ch == ' ' && !flag) {
                    list.add(str);
                    str = "";
                } else {
                    //标识旗为true时，在""范围内，遇到"空格"，加入到str
                    str = str + ch;
                }
            }
            //最后遍历结束 没有空格或引号,需要再加上sb 打印出数组的长度,即几个命令
            list.add(str);
            System.out.println(list.size());
            for (String s1 : list) {
                System.out.println(s1);
            }
        }
    }
}
