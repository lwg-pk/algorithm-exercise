package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-18 20:45
 */


import java.util.Scanner;


/**
 * 从单向链表中删除指定值的节点
 */
public class HJ48 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int n = sc.nextInt();
            int hd = sc.nextInt();
            ListNode head = new ListNode(hd);
            for (int i = 0; i < n-1; i++) {
                int insert = sc.nextInt();
                int base = sc.nextInt();
                addNode(head, insert, base);
            }

            int k = sc.nextInt();
            ListNode res = deNode(head, k);
            if (res == null) {
                System.out.println("null");
                break;
            }
            ListNode temp = res;
            while (temp != null){
                System.out.print(temp.value + " ");
                temp = temp.next;
            }
        }
    }


    public static ListNode deNode(ListNode head, int k) {
        ListNode temp = head;

        if (head.value == k && head.next == null) {
            return null;
        }
        if (head.value == k && head.next != null) {
            head = head.next;
            return head;
        }

        while (true) {
            if (temp == null) {
                return null;
            }

            if (temp.next.value == k && temp.next.next != null) {
                temp.next = temp.next.next;
                break;
            } else if (temp.next.value == k && temp.next.next == null) {
                temp.next = null;
                break;
            }
            temp = temp.next;
        }
        return head;
    }

    public static void addNode(ListNode head, int insert, int base) {
        ListNode temp = head;
        while (true) {
            if (temp == null) {
                break;
            }

            if (temp.value == base && temp.next != null) {
                ListNode node = new ListNode(insert);
                node.next = temp.next;
                temp.next = node;
                break;
            } else if (temp.value == base && temp.next == null) {
                ListNode node = new ListNode(insert);
                temp.next = node;
                break;
            }
            temp = temp.next;
        }
    }
}

class ListNode {
    int value;
    ListNode next;

    public ListNode(int value) {
        this.value = value;
        this.next = null;
    }
}


//根本不需要链表，一个有插入功能的数组就可以了。比如Java中直接用一个ArrayList即可。
//        import java.util.*;
//
//public class Main {
//    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        while (sc.hasNext()) {
//            int total = sc.nextInt();
//            int head = sc.nextInt();
//
//            List<Integer> linkedlist = new ArrayList<>();
//
//            linkedlist.add(head);
//            for (int i = 0; i < total - 1; i ++) {
//                int value = sc.nextInt();
//                int target = sc.nextInt();
//                linkedlist.add(linkedlist.indexOf(target) + 1, value);
//            }
//
//            int remove = sc.nextInt();
//            linkedlist.remove(linkedlist.indexOf(remove));
//            for (int i : linkedlist) {
//                System.out.print(i + " ");
//            }
//            System.out.println();
//        }
//    }
//}

