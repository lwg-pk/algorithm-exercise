package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-15 18:39
 */

import java.awt.font.NumericShaper;
import java.io.*;
import java.math.BigInteger;
import java.util.Scanner;

/**
 * 描述
 * 输入两个用字符串 str 表示的整数，求它们所表示的数之和。
 * 数据范围： 1≤len(str)≤10000
 * 输入描述：
 * 输入两个字符串。保证字符串只含有'0'~'9'字符
 * 输出描述：
 * 输出求和后的结果
 */
public class HJ57 {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String str1 = scanner.nextLine();
            String str2 = scanner.nextLine();
            String add = add(str1, str2);
            System.out.println(add);
        }
    }
    public static String add(String str1, String str2) {
        StringBuilder sb = new StringBuilder();
        //设置初始补位
        int carry = 0;
        int len1 = str1.length() - 1;
        int len2 = str2.length() - 1;

        while (len1 >= 0 || len2 >= 0) {
            int s1 = len1 < 0 ? 0 : str1.charAt(len1--) - '0';
            int s2 = len2 < 0 ? 0 : str2.charAt(len2--) - '0';
            int sum = s1 + s2 + carry;
            carry = sum / 10;
            int end = sum % 10;
            sb.append(String.valueOf(end));
        }
        if (carry==1){
            sb.append(String.valueOf(1));
        }
        return sb.reverse().toString();

    }

    //**********调用函数方法******
    public static BigInteger addTwo(String str1, String str2) {
        BigInteger big1 = new BigInteger(str1);
        BigInteger big2 = new BigInteger(str2);

        return big1.add(big2);

    }
}
