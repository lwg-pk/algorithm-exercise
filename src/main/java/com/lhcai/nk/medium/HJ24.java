package com.lhcai.nk.medium;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author lhcstart
 * @create 2023-02-12 22:24
 */

/**
 * 合唱队
 */
public class HJ24 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int n = sc.nextInt();
            int[] arr = new int[n];
            for (int i = 0; i < n; i++) {
                arr[i] = sc.nextInt();
            }

            //创建数组存放递增递减数据
            int[] left = new int[n];//存储每个数左边小于其的数的个数
            int[] right = new int[n];//存储每个数右边小于其的数的个数
            left[0] = 1;            //最左边的数设为1
            right[n - 1] = 1;//最右边的数设为1

            //左侧 递增
            for (int i = 0; i < n; i++) {
                left[i] = 1;//每次进入子序列循环的时候初始化为1，即为其自身
                for (int j = 0; j < i; j++) {
                    if (arr[i] > arr[j]) {
                        left[i] = Math.max(left[i], left[j] + 1);
                    }
                }
            }

            //右侧递减
            //逆序遍历，从右至左，将比arr[i]小得放入
            for (int i = n - 1; i >= 0; i--) {
                right[i] = 1;
                for (int j = n - 1; j > i; j--) {
                    if (arr[i] > arr[j]) {
                        right[i] = Math.max(right[i], right[j] + 1);
                    }
                }
            }

            //遍历求合唱队形的最大值
            int max = 1;
            for (int i = 0; i < n; i++) {
                //一个数 的 递增最大值+递减最大值 -1 （-1是因为这个数本身在递增递减中都存在）
                int num = left[i] + right[i] - 1;
                max = Math.max(max, num);
            }
            //最少需要几位同学出列
            System.out.println(n - max);

        }
    }
}
