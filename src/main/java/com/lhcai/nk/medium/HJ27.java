package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-17 11:45
 */

import java.util.*;

/**
 * 描述
 * 定义一个单词的“兄弟单词”为：交换该单词字母顺序（注：可以交换任意次），而不添加、删除、修改原有的字母就能生成的单词。
 * 兄弟单词要求和原来的单词不同。例如： ab 和 ba 是兄弟单词。 ab 和 ab 则不是兄弟单词。
 * 现在给定你 n 个单词，另外再给你一个单词 x ，让你寻找 x 的兄弟单词里，按字典序排列后的第 k 个单词是什么？
 * 注意：字典中可能有重复单词。
 * <p>
 * 数据范围： 1≤n≤1000 ，输入的字符串长度满足 1≤len(str)≤10  ，1≤k<n
 * 输入描述：
 * 输入只有一行。 先输入字典中单词的个数n，再输入n个单词作为字典单词。 然后输入一个单词x 最后后输入一个整数k
 * 输出描述：
 * 第一行输出查找到x的兄弟单词的个数m 第二行输出查找到的按照字典顺序排序后的第k个兄弟单词，没有符合第k个的话则不用输出。
 */
public class HJ27 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int n = sc.nextInt();
            String[] strs = new String[n];
            //将字典中单词放入String数组
            for (int i = 0; i < n; i++) {
                String next = sc.next();
                strs[i] = next;
            }

            String brother = sc.next();
            int k = sc.nextInt();

            List list = new ArrayList();
            int nums = 0;
            for (int i = 0; i < n; i++) {
                if (isBrother(strs[i], brother)) {
                    nums++;
                    list.add(strs[i]);
                }
            }
            Collections.sort(list);//给兄弟数组按字典排序

            if (k > nums) {
                System.out.println(nums);
            } else {
                System.out.println(nums);
                System.out.println(list.get(k - 1));
            }


        }
    }


    //判断是否是兄弟单词
    public static boolean isBrother(String str1, String str) {

        if (str1.equals(str) || str1.length() != str.length()) {
            return false;
        }
        char[] ch1 = str1.toCharArray();
        char[] ch = str.toCharArray();

        Arrays.sort(ch1);
        String s1 = String.valueOf(ch1);
        Arrays.sort(ch);
        String s = String.valueOf(ch);
        if (s1.equals(s)) {
            return true;
        }
        return false;
    }
}
