package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-17 10:51
 */

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

/**
 * 描述
 * 编写一个程序，将输入字符串中的字符按如下规则排序。
 * 规则 1 ：英文字母从 A 到 Z 排列，不区分大小写。
 * 如，输入： Type 输出： epTy
 * 规则 2 ：同一个英文字母的大小写同时存在时，按照输入顺序排列。
 * 如，输入： BabA 输出： aABb
 * 规则 3 ：非英文字母的其它字符保持原来的位置。
 * 如，输入： By?e 输出： Be?y
 * 数据范围：输入的字符串长度满足1≤n≤1000
 * 输入描述：
 * 输入字符串
 * 输出描述：
 * 输出字符串
 */
public class HJ26 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String s = sc.nextLine();
            char[] chars = s.toCharArray();
            List<Character> list = new ArrayList();

            //将所有字母放入list里
            for (char aChar : chars) {
                if (Character.isLetter(aChar)) {
                    list.add(aChar);
                }
            }
            //对字母进行排序(升序)
            list.sort(new Comparator<Character>() {
                @Override
                public int compare(Character o1, Character o2) {

                    return Character.toLowerCase(o1) - Character.toLowerCase(o2);
                }
            });

            //遍历字符串元素，添加到StringBuilder
            StringBuilder sb = new StringBuilder();
            for (int i = 0, j = 0; i < chars.length; i++) {

                if (Character.isLetter(chars[i])) {
                    sb.append(list.get(j++));
                } else {
                    sb.append(chars[i]);
                }
            }
            System.out.println(sb.toString());
        }
    }
}
