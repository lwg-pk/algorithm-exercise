package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-21 17:27
 */

import java.util.Scanner;

/**
 * 描述
 * 如果A是个x行y列的矩阵，B是个y行z列的矩阵，把A和B相乘，其结果将是另一个x行z列的矩阵C。
 * 这个矩阵的每个元素是由下面的公式决定的
 * <p>
 * 矩阵的大小不超过100*100
 * 输入描述：
 * 第一行包含一个正整数x，代表第一个矩阵的行数
 * 第二行包含一个正整数y，代表第一个矩阵的列数和第二个矩阵的行数
 * 第三行包含一个正整数z，代表第二个矩阵的列数
 * 之后x行，每行y个整数，代表第一个矩阵的值
 * 之后y行，每行z个整数，代表第二个矩阵的值
 */
public class HJ69 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        int y = sc.nextInt();
        int z = sc.nextInt();
        int[][] arr1 = new int[x][y];

        //初始化第一个数组
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                arr1[i][j] = sc.nextInt();
            }
        }

        //初始化第二个数组
        int[][] arr2 = new int[y][z];
        for (int i = 0; i < y; i++) {
            for (int j = 0; j < z; j++) {
                arr2[i][j] = sc.nextInt();
            }
        }

        //通过静态方法获取结果数组（矩阵）
        int[][] res = solution(arr1, arr2);

        for (int i = 0; i < res.length; i++) {
            for (int j = 0; j < res[0].length; j++) {
                System.out.print(res[i][j] + " ");
            }
            System.out.println();
        }


    }

    public static int[][] solution(int[][] arr1, int[][] arr2) {

        //重新获取xyz的值
        int x = arr1.length;
        int y = arr2.length;
        int z = arr2[0].length;

        //计算后的新数组
        int[][] res = new int[x][z];

        for (int i = 0; i < x; i++) {//新数组的行
            for (int j = 0; j < z; j++) {//新数组的列
                for (int k = 0; k < y; k++) {//原来矩阵的y,即第一个矩阵的y,第二个矩阵的x
                    res[i][j] += arr1[i][k] * arr2[k][j];//自加y-1次
                }
            }
        }
        return res;
    }
}
