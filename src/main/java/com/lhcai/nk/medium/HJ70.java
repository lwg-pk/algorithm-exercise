package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-20 10:16
 */

import java.util.Scanner;
import java.util.Stack;

/**
 * 矩阵乘法计算量估算
 */
public class HJ70 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int n = sc.nextInt();//矩阵个数
            int[][] a = new int[n][2];
            for (int i = 0; i < n; i++) {
                a[i][0] = sc.nextInt();//行数x
                a[i][1] = sc.nextInt();//列数y
            }
            String s = sc.next();
            Stack<Integer> stack = new Stack();
            int sum = 0;
            for (int i = 0, j = 0; i < s.length(); i++) {
                if (s.charAt(i) == '(') {
                    continue;
                } else if (s.charAt(i) == ')') {
                    Integer x1 = stack.pop();//先弹出最后放入的行
                    Integer y1 = stack.pop();//再弹出最后放入的列
                    Integer x0 = stack.pop();//先弹出前一个放入的行
                    Integer y0 = stack.pop();//再弹出前一个放入的列
                    sum += x0 * y0 * y1;
                    stack.push(y1);//先放入计算后的列
                    stack.push(x0);//再放入计算后的行

                } else {
                    stack.push(a[j][1]);//先放入列
                    stack.push(a[j][0]);//再放入行
                    j++;
                }

            }

            System.out.println(sum);
        }
    }
}
