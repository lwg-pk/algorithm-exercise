package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-18 21:36
 */


import java.util.LinkedHashMap;
import java.util.Scanner;

/**
 * 描述
 * 找出字符串中第一个只出现一次的字符
 * 数据范围：输入的字符串长度满足 1≤n≤1000
 * 输入描述：
 * 输入一个非空字符串
 * <p>
 * 输出描述：
 * 输出第一个只出现一次的字符，如果不存在输出-1
 */
public class HJ59 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {

            String s = sc.nextLine();
            char[] chars = s.toCharArray();
            LinkedHashMap<Character, Integer> map = new LinkedHashMap<>();

            for (int i = 0; i < chars.length; i++) {
                char ch = chars[i];
                map.put(ch, map.getOrDefault(ch, 0) + 1);
            }
            boolean sign = false;
            for (Character key : map.keySet()) {
                if (map.get(key) == 1) {
                    System.out.println(key.toString());
                    sign = true;
                    break;
                }
            }
            if (!sign){
                System.out.println(-1);
            }

        }
    }
}
