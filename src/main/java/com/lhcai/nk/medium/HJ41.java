package com.lhcai.nk.medium;

/**
 * @author lhcstart
 * @create 2023-02-19 15:33
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;


/**
 * 描述
 * 现有n种砝码，重量互不相等，分别为 m1,m2,m3…mn ；
 * 每种砝码对应的数量为 x1,x2,x3...xn 。现在要用这些砝码去称物体的重量(放在同一侧)，问能称出多少种不同的重量。
 * 注：
 * 称重重量包括 0
 * 数据范围：每组输入数据满足1≤n≤10  ，1≤mi≤2000  ，1≤xi≤10
 * 输入描述：
 * 对于每组测试数据：
 * 第一行：n --- 砝码的种数(范围[1,10])
 * 第二行：m1 m2 m3 ... mn --- 每种砝码的重量(范围[1,2000])
 * 第三行：x1 x2 x3 .... xn --- 每种砝码对应的数量(范围[1,10])
 * 输出描述：
 * 利用给定的砝码可以称出的不同的重量数
 */
public class HJ41 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int n = sc.nextInt();//砝码种类
            int[] weight = new int[n];//重量数组
            int[] number = new int[n];//个数数组

            //填充数组
            for (int i = 0; i < n; i++) {
                int m = sc.nextInt();
                weight[i] = m;
            }
            for (int i = 0; i < n; i++) {
                int x = sc.nextInt();
                number[i] = x;
            }


            //通过set去重
            HashSet<Integer> set = new HashSet();
            set.add(0);


            for (int i = 0; i < n; i++) {//法码的种类
                ArrayList<Integer> list = new ArrayList(set);//将每次遍历的结果放入list存放
                for (int j = 1; j <= number[i]; j++) {//砝码的个数从1-max
                    for (int k = 0; k < list.size(); k++) { //遍历list，取出存入的结果
                        //上一轮存入的结果与新加入的砝码重量相加得新结果
                        // 此时得weight[i] * j代表所有可能的  新加入砝码计算后的结果
                        set.add(list.get(k) + weight[i] * j);
                    }
                }
            }
            System.out.println(set.size());
        }

    }
}
