package com.lhcai.nk.medium;


import java.util.Scanner;

/**
 * 描述
 * 密码要求:
 * 1.长度超过8位
 * 2.包括大小写字母.数字.其它符号,以上四种至少三种
 * 3.不能有长度大于2的包含公共元素的子串重复 （注：其他符号不含空格或换行）
 * 数据范围：输入的字符串长度满足 1≤n≤100
 * 输入描述：
 * 一组字符串。
 * 输出描述：
 * 如果符合要求输出：OK，否则输出NG
 */
public class HJ20 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String pw = scanner.nextLine();
            char[] chars = pw.toCharArray();

            //判断是否符合八位
            if (chars.length > 8) {
                int a = 0, b = 0, d = 0, e = 0;
                boolean err = true;
                for (char c : chars) {
                    if (c >= '0' && c <= '9') {
                        a = 1;
                    } else if (c >= 'a' && c <= 'z') {
                        b = 1;
                    } else if (c >= 'A' && c <= 'Z') {
                        d = 1;
                    } else if (c == ' ' || c == '\n') {
                        //存在空格或者换行
                        err = false;
                        break;
                    } else {
                        e = 1;
                    }
                }

                if (err) {
                    if ((a + b + d + e) >= 3) {
                        if (restr(pw)) {
                            System.out.println("OK");
                        } else {
                            System.out.println("NG");
                        }

                    } else {
                        System.out.println("NG");
                    }

                } else {
                    System.out.println("NG");
                }


            } else {
                System.out.println("NG");
            }


        }
    }

    public static boolean restr(String s) {
        for (int i = 3; i < s.length(); i++) {
            //判断是否有公共子串重复
            if (s.substring(i).contains(s.substring(i - 3, i))) {
                return false;
            }
        }

        return true;
    }
}