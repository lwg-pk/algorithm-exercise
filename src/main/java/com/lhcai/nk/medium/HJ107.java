package com.lhcai.nk.medium;

import java.util.Scanner;

/**
 * @author lhcstart
 * @create 2023-02-20 20:46
 */
public class HJ107 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextDouble()) {
            double num = sc.nextDouble();

//            System.out.println(String.format("%.1f", solution2(num)));
            System.out.println(solution(num));
        }

    }

    public static String solution(double input) {
        double num = input > 0 ? input : -input;

        double top = 0.0;
        double bottom = 0.0;

        while (top * top * top < num) {
            top++;
        }
        bottom = top - 1;

        double mid = bottom + (top - bottom) / 2;
        double mul = mid * mid * mid;

        while (top - bottom > 0.1) {
            if (mul > num) {
                top = mid;
            } else if (mul < num) {
                bottom = mid;
            }
            mid = bottom + (top - bottom) / 2;
            mul = mid * mid * mid;
        }

        if (input < 0) {
            mid = -mid;
        }
        return String.format("%.1f", mid);
    }


    //****牛顿迭代法*************
    public static double solution2(double num) {
        if (num == 0) {
            return 0;
        }
        double x0 = num;
        double err = 1e-5;
        while (Math.abs(num - x0 * x0 * x0) >= err) {
            x0 = (2 * x0 + num / (x0 * x0)) / 3.0;
        }
        return x0;
    }
}
