package com.lhcai.nk;

/**
 * @author lhcstart
 * @create 2023-02-22 22:07
 */

/**
 * 把字符串转换成整数(atoi)
 */
public class NC100 {
    public static void main(String[] args) {
        NC100 nc = new NC100();
        int i = nc.StrToInt("82");
        System.out.println(i);
    }

    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * @param s string字符串
     * @return int整型
     */
    public int StrToInt(String s) {
        //如果为空，返回0
        if (s.isEmpty()) {
            return 0;
        }

        int index = 0;
        int res = 0;
        int n = s.length();
        //去除前导空格
        while (index < n) {
            if (s.charAt(index) == ' ') {
                index++;
            } else {
                break;
            }
        }
        //全是空格，返回0
        if (index == n) {
            return 0;
        }
        //判断符号
        int sign = 1;
        if (s.charAt(index) == '+') {
            index++;
        } else if (s.charAt(index) == '-') {
            index++;
            sign = -1;
        }
        //只有符号没有数字，输出0
        if (index == n) {
            return 0;
        }

        while (index < n) {
            char c = s.charAt(index);
            //不是数字，结束方法
            if (c < '0' || c > '9') {
                break;
            }
            //判断是否超值
            //因为res后续操作要乘10，则判断res与 Integer.MAX_VALUE / 10 的大小
            if (res > Integer.MAX_VALUE / 10 || (res == Integer.MAX_VALUE / 10 && c - '0' > Integer.MAX_VALUE % 10)) {
                return Integer.MAX_VALUE;
            }
            if (res < Integer.MIN_VALUE / 10 || (res == Integer.MIN_VALUE / 10 && c - '0' > -(Integer.MIN_VALUE % 10))) {
                return Integer.MIN_VALUE;
            }
            res = res * 10 + sign * (c - '0');
            index++;

        }
        return res;
    }
}

