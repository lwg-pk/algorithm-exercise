package com.lhcai.nk.hard;

/**
 * @author lhcstart
 * @create 2023-02-27 17:27
 */
import java.util.HashMap;
import java.util.Scanner;
/**
 * 扑克牌大小
 */
public class HJ88 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String s = sc.nextLine();
            String[] split = s.split("-");
            String s1 = split[0];
            String s2 = split[1];
            System.out.println(helper(s1, s2));
        }
    }

    public static String helper(String s1, String s2) {

        HashMap<String, Integer> map = new HashMap<>();
        map.put("A", 14);
        map.put("2", 15);
        map.put("3", 3);
        map.put("4", 4);
        map.put("5", 5);
        map.put("6", 6);
        map.put("7", 7);
        map.put("8", 8);
        map.put("9", 9);
        map.put("10", 10);
        map.put("J", 11);
        map.put("Q", 12);
        map.put("K", 13);
        map.put("joker", 16);
        map.put("JOKER", 17);

        //大小王
        if (s1.equals("joker JOKER") || s1.equals("JOKER joker")) {
            return s1;
        } else if (s2.equals("joker JOKER") || s2.equals("JOKER joker")) {
            return s2;
        }

        String[] arr1 = s1.split(" ");
        int n1 = map.get(arr1[0]);
        String[] arr2 = s2.split(" ");
        int n2 = map.get(arr2[0]);

        //炸弹
        if (isBoom(s1) && isBoom(s2)) {
            return n1 > n2 ? s1 : s2;
        } else if (isBoom(s2)) {
            return s2;
        } else if (isBoom(s1)) {
            return s1;
        } else if (arr1.length == arr2.length) {
            return n1 > n2 ? s1 : s2;
        } else {
            return "ERROR";
        }
    }

    private static boolean isBoom(String s1) {
        String[] temp = s1.split(" ");
        if (temp.length != 4) {
            return false;
        }
        String cur = temp[0];
        for (int i = 0; i < 4; i++) {
            if (!cur.equals(temp[i])) {
                return false;
            }
        }
        return true;
    }

}
