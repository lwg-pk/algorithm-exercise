package com.lhcai.nk.hard;

/**
 * @author lhcstart
 * @create 2023-02-27 22:18
 */


import java.util.HashMap;
import java.util.Scanner;

/**
 * 24点运算
 */
public class HJ89 {

    public static HashMap<String, Integer> map = new HashMap<>();
    static {
        map.put("2", 2);
        map.put("3", 3);
        map.put("4", 4);
        map.put("5", 5);
        map.put("6", 6);
        map.put("7", 7);
        map.put("8", 8);
        map.put("9", 9);
        map.put("10", 10);
        map.put("J", 11);
        map.put("Q", 12);
        map.put("K", 13);
        map.put("A", 1);
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {

            String s = sc.nextLine();
            String[] split = s.split(" ");

            if (s.contains("joker") || s.contains("JOKER")) {
                System.out.println("ERROR");
            } else {
                if (!dfs(split, 0, "", 0)) {
                    System.out.println("NONE");
                }
            }
        }
    }

    public static boolean dfs(String[] nums, int res, String exp, int n) {

        if (res == 24 && n == nums.length) {
            System.out.println(exp);
            return true;
        }

        for (int i = 0; i < nums.length; i++) {
            String temp = nums[i];

            if (!temp.equals("")) {

                nums[i] = ""; //已经使用过的牌标记为空字符串""

                Integer a = map.get(temp);
                if (n == 0) {
                    if (dfs(nums, a, exp + temp, n + 1) ||
                            dfs(nums, a, exp + temp, n + 1) ||
                            dfs(nums, a, exp + temp, n + 1) ||
                            dfs(nums, a, exp + temp, n + 1)) {
                        return true;
                    }
                }else {
                    if (dfs(nums, res + a, exp + "+" + temp, n + 1) ||
                            dfs(nums, res - a, exp + "-" + temp, n + 1) ||
                            dfs(nums, res * a, exp + "*" + temp, n + 1) ||
                            dfs(nums, res / a, exp + "/" + temp, n + 1)) {
                        return true;
                    }
                }
                nums[i]=temp;//恢复现场
            }
        }

        return false;
    }
}
