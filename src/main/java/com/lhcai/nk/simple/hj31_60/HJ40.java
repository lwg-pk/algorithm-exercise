package com.lhcai.nk.simple.hj31_60;

/**
 * @author lhcstart
 * @create 2023-02-13 22:56
 */

import java.util.Scanner;

/**
 * 描述
 * 输入一行字符，分别统计出包含英文字母、空格、数字和其它字符的个数。
 * <p>
 * 数据范围：输入的字符串长度满足 1≤n≤1000
 * 输入描述：
 * 输入一行字符串，可以有空格
 * <p>
 * 输出描述：
 * 统计其中英文字符，空格字符，数字字符，其他字符的个数
 */
public class HJ40 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNextLine()) {
            String str = sc.nextLine();
            //[^a-zA-Z]反向匹配，不包含
            String str1 = str.replaceAll("[^a-zA-Z]", "");
            String str2 = str.replaceAll("[^ ]", "");
            String str3 = str.replaceAll("[^0-9]", "");
            String str4 = str.replaceAll("[a-zA-Z0-9 ]", "");
            System.out.println(str1.length());
            System.out.println(str2.length());
            System.out.println(str3.length());
            System.out.println(str4.length());
        }
    }
}