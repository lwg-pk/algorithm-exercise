package com.lhcai.nk.simple.hj31_60;

/**
 * @author lhcstart
 * @create 2023-02-15 18:54
 */

import java.util.Scanner;

/**
 * 描述
 * 输入一个字符串和一个整数 k ，截取字符串的前k个字符并输出
 *
 * 数据范围：字符串长度满足1≤n≤1000  ，1≤k≤n
 * 输入描述：
 * 1.输入待截取的字符串
 * 2.输入一个正整数k，代表截取的长度
 *
 * 输出描述：
 * 截取后的字符串
 *
 * 示例1
 */
public class HJ46 {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        while (scanner.hasNext()){
            String s = scanner.next();
            int n = scanner.nextInt();

            String s1 = s.substring(0, n);
            System.out.println(s1);
        }
    }
}
