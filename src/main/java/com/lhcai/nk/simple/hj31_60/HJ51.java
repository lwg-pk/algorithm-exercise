package com.lhcai.nk.simple.hj31_60;

/**
 * @author lhcstart
 * @create 2023-02-13 23:12
 */

import java.util.Scanner;

/**
 * 单向链表
 */
public class HJ51 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            int n = sc.nextInt();//链表节点个数
            //头结点
            ListNode head = new ListNode(-1);
            //建立辅助节点
            ListNode temp = head;
            for (int i = 0; i < n; i++) {
                ListNode node = new ListNode(sc.nextInt());
                //新节点链接到temp上
                temp.next = node;
                //移动辅助节点
                temp = temp.next;
            }
            //获取K的值
            int k = sc.nextInt();
            System.out.println(getList(head, n, k).val);

        }
    }
    // 输出单向链表中倒数第k个结点,即正数n - k + 1
    public static ListNode getList(ListNode head, int n, int k) {
        ListNode temp = null;
        temp  =  head;
        for (int i = 0; i <= n - k; i++) {
            temp = temp.next;
        }
        return temp;
    }


}
class ListNode {
    int val;
    ListNode next;

    ListNode(int val) {
        this.val = val;
        this.next = null;
    }
}

//**************************************快慢指针****
/*
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int n = Integer.parseInt(sc.next());
            ListNode head = new ListNode(-1);
            ListNode temp = head;
            //生成链表
            for (int i = 0; i < n; i++) {
                ListNode node = new ListNode(sc.nextInt());
                temp.next = node;
                temp = temp.next;
            }
            int k = Integer.parseInt(sc.next());
            //使用快慢指针
            if(getKthFromEnd(head.next,k) != null){
                System.out.println(getKthFromEnd(head.next,k).val);
            }
            else{
                System.out.println(0);
            }

        }
    }
    //通过快慢指针搜索
    public static ListNode getKthFromEnd(ListNode head, int k) {
        if(head == null) return null;

        ListNode fast = head,slow = head;

        //快指针先走k步
        for(int i=0;i<k;i++){
            if(fast==null) return fast;
            fast = fast.next;
        }
        while(fast!=null){
            fast = fast.next;
            slow = slow.next;
        }
        return slow;
    }
}

class ListNode {
    ListNode next;
    int val;
    ListNode(int val) {
        this.val = val;
        next = null;
    }
}*/
