package com.lhcai.nk.simple.hj31_60;


import java.util.Scanner;

/**
 * @author lhcstart
 * @create 2023-02-12 22:28
 */
public class HJ31 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (sc.hasNextLine()) { // 注意 while 处理多个 case
            String s = sc.nextLine();

            StringBuilder stringBuilder =new StringBuilder();

            char[] chars = s.toCharArray();

          for (int i =0 ;i< chars.length ;i++) {

              //isLetter()判断是否是字母
              if (Character.isLetter(chars[i])){
                  stringBuilder.append(chars[i]);
              }else{
                  stringBuilder.append(" ");
              }
          }
            String string = stringBuilder.toString();
            String[] s1 = string.split(" ");

            for (int i = 0; i < s1.length; i++) {
                System.out.print(s1[s1.length -i-1] + " ");
            }


        }
    }
}
