package com.lhcai.nk.simple.hj31_60;

/**
 * @author lhcstart
 * @create 2023-02-13 20:28
 */

import java.util.*;

/**
 * 描述
 * Lily上课时使用字母数字图片教小朋友们学习英语单词，每次都需要把这些图片按照大小（ASCII码值从小到大）排列收好。请大家给Lily帮忙，通过代码解决。
 * Lily使用的图片使用字符"A"到"Z"、"a"到"z"、"0"到"9"表示。
 * <p>
 * 数据范围：每组输入的字符串长度满足  1≤n≤1000
 * 输入描述：
 * 一行，一个字符串，字符串中的每个字符表示一张Lily使用的图片。
 * <p>
 * 输出描述：
 * Lily的所有图片按照从小到大的顺序输出
 */
public class HJ34 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String s = sc.nextLine();
            char[] chars = s.toCharArray();
            Arrays.sort(chars);
            System.out.println(String.valueOf(chars));

        }
    }


    public static void sortWord(String s) {
        //创建数组，初始值为零
        int[] num = new int[128];
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            int k = (int) chars[i];//统计出现的次数
            num[k]++;
        }

        for (int i = 48; i < num.length; i++) {//从'0'开始输出
            if (num[i] != 0) {
                for (int b = 0 ;b < num[i];b++)
                System.out.print((char)i);
            }
        }


    }
}
