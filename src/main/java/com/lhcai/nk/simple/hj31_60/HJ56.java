package com.lhcai.nk.simple.hj31_60;

/**
 * @author lhcstart
 * @create 2023-02-15 18:19
 */

import java.util.Scanner;;

/**
 * 描述
 * 完全数（Perfect number），又称完美数或完备数，是一些特殊的自然数。
 * 它所有的真因子（即除了自身以外的约数）的和（即因子函数），恰好等于它本身。
 * 例如：28，它有约数1、2、4、7、14、28，除去它本身28外，其余5个数相加，1+2+4+7+14=28。
 * 输入n，请输出n以内(含n)完全数的个数。
 * 数据范围：1≤n≤5×10^5
 * 输入描述： 输入一个数字n
 * 输出描述： 输出不超过n的完全数的个数
 */
public class HJ56 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            int n = scanner.nextInt();
            if (n < 6) {
                System.out.println(0);
                continue;
            }

            int count = 0;
            for (int i = 6; i <= n; i++) {
                int sum = 0;
                for (int j = 1; j <= i / 2; j++) {
                    if (i % j == 0) {
                        sum += j;
                    }
                }
                if (sum == i) {
                    count++;
                }
            }
            System.out.println(count);
        }

    }
}
