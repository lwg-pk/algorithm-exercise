package com.lhcai.nk.simple.hj31_60;

/**
 * @author lhcstart
 * @create 2023-02-15 20:03
 */


import java.util.Scanner;

/**
 * 描述
 * 任意一个偶数（大于2）都可以由2个素数组成，组成偶数的2个素数有很多种情况，
 * 本题目要求输出组成指定偶数的两个素数差值最小的素数对。
 * 数据范围：输入的数据满足4≤n≤1000
 * 输入描述：
 * 输入一个大于2的偶数
 * <p>
 * 输出描述：
 * 从小到大输出两个素数
 */
public class HJ60 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        solution(n);

    }

    public static void solution(int num) {
        for (int i = num / 2; i < num - 1; i++) {
            if (isPrimary(i) && isPrimary(num - i)) {
                System.out.println((num - i) + "\n" + i);
                break;
            }
        }

    }


    public static boolean isPrimary(int n) {
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
