package com.lhcai.nk.simple.hj31_60;

/**
 * @author lhcstart
 * @create 2023-02-15 10:38
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 杨辉三角
 */
//***************非找规律法*******
public class HJ53 {
    public static List<List<Integer>> res = new ArrayList<>();

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[][] arr = new int[n][];
        for(int i = 0; i < arr.length; i++){
            arr[i] = new int[i+1];
            arr[i][0] = 1;
            for (int j = 1; j < arr[i].length; j++) {
                if(i <= 1){
                    arr[i][j] = 1;
                } else {
                    if(j == 1){
                        arr[i][j] = arr[i-1][j-1] + arr[i-1][j];
                    }
                    else if(j == arr[i].length - 1){
                        arr[i][j] = (arr[i-1][j-2] * 2) + arr[i-1][j-1];
                    }
                    else{
                        arr[i][j] = arr[i-1][j-2] + arr[i-1][j-1] + arr[i-1][j];
                    }
                }
            }
        }
        int index = -1;
        for (int j = 0; j < n ; j++) {
            if (arr[n - 1][j] % 2 == 0){
                index = j + 1;
                break;
            }
        }
        System.out.println(index);
    }
}
//**************找规律法******
/*
public class HJ53 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            int n = sc.nextInt();
            if (n == 1 || n == 2) {
                System.out.println(-1);
                continue;
            } else if (n % 2 == 1) {
                System.out.println(2);
                continue;
            } else if (n % 4 == 0) {
                System.out.println(3);
                continue;
            } else if (n % 4 == 2) {
                System.out.println(4);
                continue;
            }
        }
    }
}
*/

