package com.lhcai.nk.simple.hj31_60;

/**
 * @author lhcstart
 * @create 2023-02-15 11:35
 */

import java.util.Scanner;
import java.util.Stack;


/**
 *
 */
public class HJ54 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String s = sc.nextLine();
            //将其他括号换成小括号
            s = s.replace("{", "(");
            s = s.replace("[", "(");
            s = s.replace("}", ")");
            s = s.replace("]", ")");
            int slove = slove(s);
            System.out.println(slove);
        }
    }

    public static int slove(String s) {
        //初始化栈
        Stack<Integer> stack = new Stack<>();
        //获取字符串长度
        int n = s.length();
        //定义初始符号
        char sign = '+';
        //获得字符数组
        char[] chars = s.toCharArray();
        //定义初始数
        int number = 0;
        for (int i = 0; i < n; i++) {
            //空格，结束当前循环
            if (chars[i] == ' ') {
                continue;
            }

            //元素是数字
            if (Character.isDigit(chars[i])) {
                //叠加数字
                number = number * 10 + chars[i] - '0';
            }
            //元素是小括号
            if (chars[i] == '(') {
                int j = i + 1;
                int count = 1;
                while (count > 0) {
                    if (chars[j] == '(') {
                        count++;
                    }
                    if (chars[j] == ')') {
                        count--;
                    }
                    j++;
                }
                //递归，解小括号中的表达式
                number = slove(s.substring(i + 1, j - 1));
                i = j - 1;
            }

            //遇到符号，将数字处理后放进栈
            if (!Character.isDigit(chars[i]) || i == n - 1) {
                //如果是'+',直接入栈
                if (sign == '+') {
                    stack.push(number);
                    //如果是'-',数字取相反数在入栈
                } else if (sign == '-') {
                    stack.push(-1 * number);
                    //如果是'*',弹出一个数字乘后放入栈
                } else if (sign == '*') {
                    stack.push(stack.pop() * number);
                    //如果是'/',弹出一个数字/后放入栈
                } else if (sign == '/') {
                    stack.push(stack.pop() / number);
                }
                //更新符号
                sign = chars[i];
                //更新数字
                number = 0;
            }
        }

        //栈中数字求和得结果
        int ans = 0;
        while (!stack.isEmpty()) {
            ans += stack.pop();
        }

        return ans;
    }
}
