package com.lhcai.nk.simple.hj1_30;

/**
 * @author lhcstart
 * @create 2023-02-10 23:26
 */

import java.util.Scanner;

/**
 * 描述
 * 功能:输入一个正整数，按照从小到大的顺序输出它的所有质因子（重复的也要列举）（如180的质因子为2 2 3 3 5 ）
 * <p>
 * <p>
 * 数据范围：1≤n≤2×10^9 + 14
 * 输入描述：
 * 输入一个整数
 * <p>
 * 输出描述：
 * 按照从小到大的顺序输出它的所有质数的因子，以空格隔开。
 */
public class HJ6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLong()) {
            long num = sc.nextLong();


            for (int i = 2; i * i <= num; i++) {
                while (num % i == 0) {
                    System.out.print(i + " ");

                    num /= i;
                }
            }

            System.out.println(num == 1 ? "" : num + "");


        }
    }
}
