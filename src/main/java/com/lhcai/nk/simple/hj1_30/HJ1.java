package com.lhcai.nk.simple.hj1_30;

/**
 * @author lhcstart
 * @create 2023-02-10 22:11
 */

import java.util.Scanner;

/**
 * 牛客第一题：
 * 计算字符串最后一个单词的长度，单词以空格隔开，字符串长度小于5000。（注：字符串末尾不以空格为结尾）
 * 输入描述：
 * 输入一行，代表要计算的字符串，非空，长度小于5000。
 *
 * 输出描述：
 * 输出一个整数，表示输入字符串最后一个单词的长度。
 */
public class HJ1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        //获取输入的一行字符串
        String str = sc.nextLine();
        //将获取的字符串以空格分隔，存入数组
        String[] strArray = str.split(" ");
        //获取数组的最后一个元素的长度
        int length = strArray[strArray.length - 1].length();
        System.out.println(length);
    }
}
