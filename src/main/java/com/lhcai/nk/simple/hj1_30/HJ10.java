package com.lhcai.nk.simple.hj1_30;

/**
 * @author lhcstart
 * @create 2023-02-11 16:33
 */

import java.util.Scanner;

/**
 * 描述
 * 编写一个函数，计算字符串中含有的不同字符的个数。字符在 ASCII 码范围内( 0~127 ，包括 0 和 127 )，换行表示结束符，不算在字符里。不在范围内的不作统计。多个相同的字符只计算一次
 * 例如，对于字符串 abaca 而言，有 a、b、c 三种不同的字符，因此输出 3 。
 * <p>
 * 数据范围：1≤n≤500
 * 输入描述：
 * 输入一行没有空格的字符串。
 * <p>
 * 输出描述：
 * 输出 输入字符串 中范围在(0~127，包括0和127)字符的种数。
 */
public class HJ10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        char[] chars = str.toCharArray();
        int num = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.indexOf(str.charAt(i)) == i && str.charAt(i) >= 0 && str.charAt(i) <= 127) {
                num++;
            }

        }
        System.out.print(num);
    }
}
