package com.lhcai.nk.simple.hj1_30;

/**
 * @author lhcstart
 * @create 2023-02-11 15:50
 */

import java.util.*;

/**
 * 描述
 * 输入一个 int 型整数，按照从右向左的阅读顺序，返回一个不含重复数字的新的整数。
 * 保证输入的整数最后一位不是 0 。
 * <p>
 * 数据范围：1 ≤ n ≤ 10^8
 * <p>
 * 输入描述：
 * 输入一个int型整数
 * <p>
 * 输出描述：
 * 按照从右向左的阅读顺序，返回一个不含重复数字的新的整数
 */
public class HJ9 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //逆序排列
        StringBuilder stringBuilder = new StringBuilder(scanner.nextLine()).reverse();
        String str = stringBuilder.toString();
        //结果集
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            if (str.indexOf(str.charAt(i)) == i) {//获取第一次搜索到值的索引
                result.append(str.charAt(i));
            }
        }
        System.out.println(result.toString());
    }


    public void reverse(int i){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            // 使用HashSet来判断是否是不重复的
            HashSet<Integer> hs = new HashSet<>();
            int target = sc.nextInt();// 获取代求解的值
            while(target != 0){ // 求解每位上面的整数
                int temp = target % 10;
                if(hs.add(temp)) // 如果能加入，就是说明没有重复
                    System.out.print(temp);
                target /= 10;// 除10能去掉最右边的数字
            }
            System.out.println();
        }

    }
}
