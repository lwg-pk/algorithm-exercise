package com.lhcai.nk.simple.hj1_30;

/**
 * @author lhcstart
 * @create 2023-02-11 16:48
 */

import java.util.Scanner;

/**
 * 描述
 * 输入一个整数，将这个整数以字符串的形式逆序输出
 * 程序不考虑负数的情况，若数字含有0，则逆序形式也含有0，如输入为100，则输出为001
 * 数据范围：0≤n≤2^30−1
 * 输入描述：
 * 输入一个int整数
 * <p>
 * 输出描述：
 * 将这个整数以字符串的形式逆序输出
 */
public class HJ11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        StringBuilder stringBuilder = new StringBuilder(scanner.nextLine()).reverse();

        String str = stringBuilder.toString();
        System.out.println(str);
    }

    public void reverse() {
        Scanner sc = new Scanner(System.in);
        int in = sc.nextInt();
        String str = String.valueOf(in);
        for (int i = str.length() - 1; i >= 0; i--) {
            System.out.print(str.charAt(i));
        }
    }
}
