package com.lhcai.nk.simple.hj1_30;

/**
 * @author lhcstart
 * @create 2023-02-12 21:36
 */


import java.util.Scanner;

/**
 * 描述
 * 某商店规定：三个空汽水瓶可以换一瓶汽水，允许向老板借空汽水瓶（但是必须要归还）。
 * 小张手上有n个空汽水瓶，她想知道自己最多可以喝到多少瓶汽水。
 * 数据范围：输入的正整数满足1≤n≤100
 * 注意：本题存在多组输入。输入的 0 表示输入结束，并不用输出结果。
 * 输入描述：
 * 输入文件最多包含 10 组测试数据，每个数据占一行，仅包含一个正整数 n（ 1<=n<=100 ），表示小张手上的空汽水瓶数。n=0 表示输入结束，你的程序不应当处理这一行。
 * <p>
 * 输出描述：
 * 对于每组测试数据，输出一行，表示最多可以喝的汽水瓶数。如果一瓶也喝不到，输出0。
 */
public class HJ22 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (sc.hasNextInt()) { // 注意 while 处理多个 case
            int n = sc.nextInt();
            if (n == 0) {
                return;
            }else{
                countCola(n,0);
            }



        }
    }

   private static void countCola(int num ,int count){
        int k = num / 3;//喝
       int v = num % 3;
       count = count + k;
       num =  k + v;
       //剩一个空瓶 ，喝不了
       if (num == 1){
           System.out.println(count);
           return;
       }
       //剩两个 ，借一喝一
       if(num == 2 || num ==3){
           System.out.println(count +1);
           return;
       }else{
           countCola(num, count);
       }



   }
}
