package com.lhcai.nk.simple.hj1_30;

/**
 * @author lhcstart
 * @create 2023-02-11 19:31
 */

import java.util.Scanner;

/**
 * 描述
 * 输入一个 int 型的正整数，计算出该 int 型数据在内存中存储时 1 的个数。
 *
 * 数据范围：保证在 32 位整型数字范围内
 * 输入描述：
 *  输入一个整数（int类型）
 *
 * 输出描述：
 *  这个数转换成2进制后，输出1的个数
 */
public class HJ15 {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);

        int num = scanner.nextInt();

        String str = Integer.toBinaryString(num);

        String str1 = str.replace("1", "");
        System.out.println(str.length() - str1.length());


    }


}
