package com.lhcai.nk.simple.hj1_30;

import java.util.Scanner;

/**
 * @author lhcstart
 * @create 2023-02-12 20:56
 */
public class HJ21 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String pw = scanner.nextLine();
            char[] ch = pw.toCharArray();
            StringBuilder sb = new StringBuilder();

            for (char c : ch) {

                if (c == '1') {
                    System.out.print("1");
                } else if (c == '0') {
                    System.out.print("0");
                } else if (c >= 'a' && c <= 'c') {
                    System.out.print("2");
                } else if (c >= 'd' && c <= 'f') {
                    System.out.print("3");
                } else if (c >= 'g' && c <= 'i') {
                    System.out.print("4");
                } else if (c >= 'j' && c <= 'l') {
                    System.out.print("5");
                } else if (c >= 'm' && c <= 'o') {
                    System.out.print("6");
                } else if (c >= 'p' && c <= 's') {
                    System.out.print("7");
                } else if (c >= 't' && c <= 'v') {
                    System.out.print("8");
                } else if (c >= 'w' && c <= 'z') {
                    System.out.print("9");
                } else if (c >= 'A' && c <= 'Y') {
                    char v =(char)(c + 1);
                    String s = String.valueOf(v).toLowerCase();
                    System.out.print(s);
                }else if (c == 'Z'){
                    System.out.print("a");
                }else{
                    System.out.print(c);
                }

            }

        }
    }
}
