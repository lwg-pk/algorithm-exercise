package com.lhcai.nk.simple.hj1_30;

import java.util.Scanner;

/**
 * @author lhcstart
 * @create 2023-02-10 23:35
 */

/**
 * 描述
 * 写出一个程序，接受一个正浮点数值，输出该数值的近似整数值。如果小数点后数值大于等于 0.5 ,向上取整；小于 0.5 ，则向下取整。
 *
 * 数据范围：保证输入的数字在 32 位浮点数范围内
 * 输入描述：
 * 输入一个正浮点数值
 *
 * 输出描述：
 * 输出该数值的近似整数值
 */
public class HJ7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while (sc.hasNextFloat()) {
            float f = sc.nextFloat();

            float x = f - (int) f;

            int z = (int) f;

            if (x >= 0.5) {
                System.out.println(z + 1);
            } else {
                System.out.println(z );
            }
        }
    }


}

