package com.lhcai.nk.simple.hj1_30;

/**
 * @author lhcstart
 * @create 2023-02-12 22:01
 */



import java.util.*;

/**
 * 描述
 * 实现删除字符串中出现次数最少的字符，若出现次数最少的字符有多个，则把出现次数最少的字符都删除。输出删除这些单词后的字符串，字符串中其它字符保持原来的顺序。
 * <p>
 * 数据范围：输入的字符串长度满足 1≤n≤20  ，保证输入的字符串中仅出现小写字母
 * 输入描述：
 * 字符串只包含小写英文字母, 不考虑非法输入，输入的字符串长度小于等于20个字节。
 * <p>
 * 输出描述：
 * 删除字符串中出现次数最少的字符后的字符串。
 */
public class HJ23 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String s = scanner.nextLine();
            char[] chars = s.toCharArray();
            Map<Character, Integer> map = new HashMap<>();
            for (int i = 0; i < chars.length; i++) {
                map.put(chars[i], map.getOrDefault(chars[i], 0) + 1);
            }
            int min = Integer.MAX_VALUE;
            for (Integer value : map.values()) {
                if (value <= min) {
                    min = value;
                }
            }
            for (Character ch : map.keySet()) {

                if (map.get(ch) == min){
                     s = s.replaceAll(String.valueOf(ch), "");
                }
            }
            System.out.println(s);
        }

//************获取最小值的方法不一样********
        //        String str = scanner.nextLine();
//        //统计每个字母的数量
//        Map<Character, Integer> map = new HashMap<>();
//        char[] chars = str.toCharArray();
//
//        for (char ac : chars) {
//            map.put(ac, (map.getOrDefault(ac, 0) + 1));
//        }
//        //找到数量最少的字符数量
//        Collection<Integer> values = map.values();
//        Integer min = Collections.min(values);
//
//        //用空字符串替换该字母
//        for (Character character : map.keySet()) {
//            if (map.get(character) == min) {
//                str = str.replaceAll(String.valueOf(character), "");
//            }
//        }
//        System.out.println(str);
    }
}
