package com.lhcai.nk.simple.hj60_;
/**
 * @author lhcstart
 * @create 2023-02-16 17:09
 */


import java.util.Scanner;

/**
 * 描述
 * 请计算n*m的棋盘格子（n为横向的格子数，m为竖向的格子数）从棋盘左上角出发沿着边缘线从左上角走到右下角，总共有多少种走法，要求不能走回头路，即：只能往右和往下走，不能往左和往上走。
 * 注：沿棋盘格之间的边缘线行走
 * 数据范围：1≤n,m≤8
 * 输入描述：
 * 输入两个正整数n和m，用空格隔开。(1≤n,m≤8)
 * <p>
 * 输出描述：
 * 输出一行结果
 */
public class HJ91 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            int n = sc.nextInt();//列数
            int m = sc.nextInt();//行数

            int[][] dp = new int[m + 1][n + 1];

            for (int i = 0; i <= m; i++) {
                for (int j = 0; j <= n; j++) {
                    if (i == 0 || j == 0){
                        dp[i][j] =1;
                    }else {
                        dp[i][j] = dp[i-1][j] + dp[i][j-1];
                    }
                }
            }
            System.out.println(dp[m][n]);

        }
    }
}

/*
//***********递归的解法**********************
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            int m = sc.nextInt();
            int n = sc.nextInt();
            System.out.println(cal(m,n));
        }

    }
     private static int cal(int m,int n){
            if(m==1 || n== 1){
                return m+n;
            }
            return cal(m-1,n)+cal(m,n-1);
        }
}
 */