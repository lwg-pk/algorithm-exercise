package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-15 19:45
 */



import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

/**
 * 描述
 * 输入整型数组和排序标识，对其元素按照升序或降序进行排序
 * <p>
 * 数据范围：1≤n≤1000  ，元素大小满足 0≤val≤100000
 * 输入描述：
 * 第一行输入数组元素个数
 * 第二行输入待排序的数组，每个数用空格隔开
 * 第三行输入一个整数0或1。0代表升序排序，1代表降序排序
 * <p>
 * 输出描述：
 * 输出排好序的数字
 */
public class HJ101 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            int n = sc.nextInt();
            Integer[] arr = new Integer[n];
            for (int i = 0; i < n; i++) {
                arr[i] = sc.nextInt();
            }
            int flag = sc.nextInt();
           sortForFlag(arr,flag);
            for (Integer integer : arr) {
                System.out.print(integer + " ");
            }
            System.out.println();

        }

    }

    public static void sortForFlag(Integer[] arr, int flag) {

        if (flag == 0) {
            Arrays.sort(arr, new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    return o1-o2;
                }
            });
        }

        if (flag == 1) {
            Arrays.sort(arr, new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    return o2 -o1;
                }
            });
        }


    }

}

