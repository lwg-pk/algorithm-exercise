package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-16 11:18
 */

import java.util.Scanner;

/**
 * 描述
 * 判断短字符串S中的所有字符是否在长字符串T中全部出现。
 * 请注意本题有多组样例输入。
 * 数据范围:1≤len(S),len(T)≤200
 * 进阶：时间复杂度：O(n) ，空间复杂度：O(n)
 * 输入描述：
 * 输入两个字符串。第一个为短字符串，第二个为长字符串。两个字符串均由小写字母组成。
 *
 * 输出描述：
 * 如果短字符串的所有字符均在长字符串中出现过，则输出字符串"true"。否则输出字符串"false"。
 */
public class HJ81 {
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        while (sc.hasNextLine()){
            char[] chars = sc.nextLine().toCharArray();

            String t = sc.nextLine();
            int count=0;
            for (int i = 0; i < chars.length; i++) {
                if (t.contains(String.valueOf( chars[i])))
                count++;
            }

            if (count == chars.length){
                System.out.println(true);
            }else {
                System.out.println(false);
            }


        }
    }
}
