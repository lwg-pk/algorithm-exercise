package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-15 22:08
 */

import javax.sound.midi.Soundbank;
import java.util.Scanner;

/**
 * 描述
 * 输入一个正整数，计算它在二进制下的1的个数。
 * 注意多组输入输出！！！！！！
 * <p>
 * 数据范围：1≤n≤ 2^31−1
 * 输入描述：
 * 输入一个整数
 * <p>
 * 输出描述：
 * 计算整数二进制中1的个数
 */
public class HJ62 {
    public static void main(String[] args) {

        //*********非调用API*****
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();    //读取数字
        int n = 0;    //计数变量
        for (int i = 0; i < 32; i++) {
            if ((num & 1) == 1)    //如果末位为1则计数
                n++;
            num = num >>> 1;    //无符号右移
        }
        System.out.println(n);
    }
}

/*
import java.util.Scanner;

public class Main{
    public static void main(String[] args){

         Scanner sc=new Scanner(System.in);
        while (sc.hasNextInt()){

            int s = sc.nextInt();

            String str = Integer.toBinaryString(s);

            String str1 = str.replace("1", "");
            System.out.println(str.length()-str1.length());


        }
    }
}
 */
