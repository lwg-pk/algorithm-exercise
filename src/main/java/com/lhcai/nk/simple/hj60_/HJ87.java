package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-16 16:09
 */


import java.util.Scanner;

/**
 * 密码强度
 */
public class HJ87 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            char[] chars = sc.nextLine().toCharArray();
            int n = chars.length;
            int count = 0;
            //统计长度
            if (n <= 4) {
                count += 5;
            } else if (n > 4 && n < 8) {
                count += 10;
            } else {
                count += 25;
            }
            //遍历获取大小写字母、数字、符号数目
            int upCount = 0;
            int lowCount = 0;
            int numCount = 0;
            int sigCount = 0;
            for (int i = 0; i < n; i++) {
                if (Character.isUpperCase(chars[i])) {
                    upCount++;
                } else if (Character.isLowerCase(chars[i])) {
                    lowCount++;
                } else if (Character.isDigit(chars[i])) {
                    numCount++;
                } else {
                    sigCount++;
                }
            }
            //字母分数
            if ((upCount > 0 && lowCount == 0) || (upCount == 0 && lowCount > 0)) {
                count += 10;
            } else if (upCount > 0 && lowCount > 0) {
                count += 20;
            } else {
                count += 0;
            }
            //数字分数
            if (numCount > 1) {
                count += 20;
            } else if (numCount == 1) {
                count += 10;
            } else {
                count += 0;
            }
            //符号分数
            if (sigCount > 1) {
                count += 25;
            } else if (sigCount == 1) {
                count += 10;
            } else {
                count += 0;
            }
            //奖励分数
            if (upCount > 0 && lowCount > 0 && numCount > 0 && sigCount > 0) {
                count += 5;
            } else if ((upCount > 0 || lowCount > 0) && numCount > 0 && sigCount > 0) {
                count += 3;
            } else if (numCount > 0 && (upCount > 0 || lowCount > 0)) {
                count += 2;
            }

            if (count >= 90) {
                System.out.println("VERY_SECURE");
            } else if (count >= 80) {
                System.out.println("SECURE");
            } else if (count >= 70) {
                System.out.println("VERY_STRONG");
            } else if (count >= 60) {
                System.out.println("STRONG");
            } else if (count >= 50) {
                System.out.println("AVERAGE");
            } else if (count >= 25) {
                System.out.println("WEAK");
            } else {
                System.out.println("VERY_WEAK");
            }


        }
    }
}
