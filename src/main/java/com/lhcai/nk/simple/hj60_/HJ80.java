package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-16 10:59
 */

import java.util.*;

/**
 * 描述
 * 题目标题：
 * <p>
 * 将两个整型数组按照升序合并，并且过滤掉重复数组元素。
 * 输出时相邻两数之间没有空格。
 * <p>
 * 输入描述：
 * 输入说明，按下列顺序输入：
 * 1 输入第一个数组的个数
 * 2 输入第一个数组的数值
 * 3 输入第二个数组的个数
 * 4 输入第二个数组的数值
 * <p>
 * 输出描述：
 * 输出合并之后的数组
 */
public class HJ80 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            int n = sc.nextInt();

            TreeSet<Integer> treeSet = new TreeSet<>();//treeSet去重并且默认排序

            for (int i = 0; i < n; i++) {
                int one = sc.nextInt();
                treeSet.add(one);
            }
            int m = sc.nextInt();

            for (int i = 0; i < m; i++) {
                int two = sc.nextInt();
                treeSet.add(two);
            }

            Iterator<Integer> iterator =treeSet.iterator();
            while (iterator.hasNext()){
                System.out.print(iterator.next());
            }
            System.out.println();
        }
    }
}
