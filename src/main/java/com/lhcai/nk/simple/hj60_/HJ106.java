package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-17 10:10
 */

import java.util.Scanner;

/**
 * 描述
 * 将一个字符串str的内容颠倒过来，并输出。
 *
 * 数据范围：1≤len(str)≤10000
 * 输入描述：
 * 输入一个字符串，可以有空格
 *
 * 输出描述：
 * 输出逆序的字符串
 */
public class HJ106 {
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        while (sc.hasNextLine()){

            String s = sc.nextLine();
            StringBuilder sb = new StringBuilder(s);
            StringBuilder reverse = sb.reverse();
            String string = reverse.toString();
            System.out.println(string);


        }
    }
}
