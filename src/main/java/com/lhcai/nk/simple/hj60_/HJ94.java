package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-16 19:36
 */

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * 描述
 * 请实现一个计票统计系统。你会收到很多投票，其中有合法的也有不合法的，请统计每个候选人得票的数量以及不合法的票数。
 * （注：不合法的投票指的是投票的名字不存在n个候选人的名字中！！）
 * <p>
 * 数据范围：每组输入中候选人数量满足  1≤n≤100  ，总票数量满足1≤n≤100
 * 输入描述：
 * 第一行输入候选人的人数n，第二行输入n个候选人的名字（均为大写字母的字符串），第三行输入投票人的人数，第四行输入投票。
 * <p>
 * 输出描述：
 * 按照输入的顺序，每行输出候选人的名字和得票数量（以" : "隔开，注：英文冒号左右两边都有一个空格！），最后一行输出不合法的票数，格式为"Invalid : "+不合法的票数。
 */
public class HJ94 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            //候选人个数
            int n = sc.nextInt();

            //将候选人放入map中作为key,value初始值为0
            Map<String, Integer> map = new LinkedHashMap<>();
            for (int i = 0; i < n; i++) {
                String str = sc.next();
                map.put(str, map.getOrDefault(str, 0));
            }

            //投票人个数
            int m = sc.nextInt();//投票人
            //遍历票单，不合格的Invalid+1；合格进行判断放入map
            int Invalid = 0;

            for (int i = 0; i < m; i++) {
                String key = sc.next();
                if (!map.containsKey(key)) {
                    Invalid++;
                } else {
                    map.put(key, map.get(key) + 1);
                }
            }
            for (String str : map.keySet()) {
                System.out.println(str + " : " + map.get(str));
            }
            System.out.println("Invalid" + " : " + Invalid);
        }
    }
}
