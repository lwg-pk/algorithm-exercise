package com.lhcai.nk.simple.hj60_;

import java.util.Calendar;
import java.util.Scanner;

/**
 * @author lhcstart
 * @create 2023-02-15 23:12
 */
/*
描述
根据输入的日期，计算是这一年的第几天。
保证年份为4位数且日期合法。
进阶：时间复杂度：O(n) ，空间复杂度：O(1)
输入描述：
输入一行，每行空格分割，分别是年，月，日

输出描述：
输出是这一年的第几天
 */
public class HJ73 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            int year = sc.nextInt();
            int month = sc.nextInt();
            int day = sc.nextInt();
//            dayForYear(year, month, day);
            dayOfYear(year, month, day);

        }
    }
//*******非调用函数的方法****

    public static void dayOfYear(int year, int month, int day) {

        int nums = 0;
        int[] month_day = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        for (int i = 0; i < month - 1; i++) {
            nums += month_day[i];
        }
        nums += day;

        if ((year % 400 == 0) || (year % 100 != 0 && year % 4 == 0)) {

            if (month > 2) {
                nums += 1;
            }
        }
        System.out.println(nums);
    }


    //**********调用函数**
    public static void dayForYear(int year, int month, int day) {
        Calendar instance = Calendar.getInstance();
        instance.set(year, month - 1, day);

        System.out.println(instance.get(instance.DAY_OF_YEAR));

    }

}
