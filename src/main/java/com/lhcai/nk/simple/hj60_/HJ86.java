package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-16 15:49
 */

import java.util.Scanner;
import java.util.zip.CheckedOutputStream;

/**
 * 描述
 * 求一个int类型数字对应的二进制数字中1的最大连续数，例如3的二进制为00000011，最大连续2个1
 * <p>
 * 数据范围：数据组数： 1≤t≤5 ，1≤n≤500000
 * 进阶：时间复杂度： O(logn) ，空间复杂度： O(1)
 * 输入描述：
 * 输入一个int类型数字
 * <p>
 * 输出描述：
 * 输出转成二进制之后连续1的个数
 */
public class HJ86 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {

            int n = sc.nextInt();
            int max = 0;
            int count = 0;
            for (int i = 0; i < 32; i++) {
                if ((n & 1) == 1) {
                    count++;
                    max = Math.max(max, count);
                } else {
                    count = 0;
                }
                n = n >>> 1;
            }
            System.out.println(max);
        }
        System.out.println();

    }
}
