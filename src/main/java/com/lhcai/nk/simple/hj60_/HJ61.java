package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-15 21:45
 */


import javax.sound.midi.Soundbank;
import java.util.Scanner;

/**
 * 描述
 * 把m个同样的苹果放在n个同样的盘子里，允许有的盘子空着不放，问共有多少种不同的分法？
 * 注意：如果有7个苹果和3个盘子，（5，1，1）和（1，5，1）被视为是同一种分法。
 * 数据范围：0≤m≤10 ，1≤n≤10 。
 * 输入描述：
 * 输入两个int整数
 * <p>
 * 输出描述：
 * 输出结果，int型
 */
public class HJ61 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            int m = sc.nextInt();
            int n = sc.nextInt();
            System.out.println(solution(m, n));

        }
    }

    public static int solution(int m, int n) {
        if (m < 0 || n < 0) {
            return 0;
        }

        if (m == 1 || n == 1) {
            return 1;
        }

        return solution(m, n - 1) + solution(m - n, n);

    }


}
