package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-16 20:37
 */

import java.util.Scanner;

/**
 * 描述
 * 将一个字符串中所有的整数前后加上符号“*”，其他字符保持不变。连续的数字视为一个整数。
 * 数据范围：字符串长度满足 1≤n≤100
 * 输入描述：
 * 输入一个字符串
 * <p>
 * 输出描述：
 * 字符中所有出现的数字前后加上符号“*”，其他字符保持不变
 */
public class HJ96 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String s = sc.nextLine();
            StringBuilder sb = new StringBuilder();
            boolean sign = false;
            for (int i = 0; i < s.length(); i++) {
                char c = s.charAt(i);
                if (c >= '0' && c <= '9'){
                    if (!sign){
                        sb.append("*");
                    }
                    sb.append(c);
                    sign = true;
                }else {
                    if (sign){
                        sb.append("*");
                    }
                    sb.append(c);
                    sign = false;
                }
            }
            if (sign){
                sb.append("*");
            }
            System.out.println(sb.toString());
        }
    }
}
/*
//********大神解法，正则表达式
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String input = scanner.next();
            System.out.println(input.replaceAll("([0-9]+)", "*$1*")); //把所有的数字段提取出来，前后加上星号再放回去
        }
    }
}
*/