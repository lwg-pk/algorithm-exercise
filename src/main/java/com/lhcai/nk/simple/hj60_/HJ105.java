package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-16 22:41
 */

import javax.xml.transform.Source;
import java.util.Scanner;

/**
 * 描述
 * 输入 n 个整型数，统计其中的负数个数并求所有非负数的平均值，结果保留一位小数，如果没有非负数，则平均值为0
 * 本题有多组输入数据，输入到文件末尾。
 * <p>
 * 数据范围：1≤n≤50000  ，其中每个数都满足∣val∣≤10^6
 * <p>
 * 输入描述：
 * 输入任意个整数，每行输入一个。
 * <p>
 * 输出描述：
 * 输出负数个数以及所有非负数的平均值
 */
public class HJ105 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int countF = 0;
        int countZ = 0;
        int sum = 0;
        double avl =0.0;
        while (sc.hasNextInt()) {
            int n = sc.nextInt();
            if (n < 0) {
                countF++;
            } else if (n > 0) {
                countZ++;
                sum += n;
            }
        }
        avl = countZ == 0 ? 0.0 : sum * 1.0 / countZ;
        System.out.println(countF);
        System.out.println(String.format("%.1f", avl));
    }
}
