package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-17 10:15
 */

import java.util.Scanner;

/**
 * 描述
 * 正整数A和正整数B 的最小公倍数是指 能被A和B整除的最小的正整数值，设计一个算法，求输入A和B的最小公倍数。
 * <p>
 * 数据范围：
 * 1≤a,b≤100000
 * 输入描述：
 * 输入两个正整数A和B。
 * <p>
 * 输出描述：
 * 输出A和B的最小公倍数。
 */
public class HJ108 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            int a = sc.nextInt();
            int b = sc.nextInt();
            int max = Math.max(a, b);


            for (int i = max; i <= a * b; i++) {
                if (i % a == 0 && i % b == 0) {
                    System.out.println(i);
                    break;
//                    return;
                }
            }
        }
    }
}
