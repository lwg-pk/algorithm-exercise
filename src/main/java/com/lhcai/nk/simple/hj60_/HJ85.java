package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-16 11:55
 */

import java.util.Scanner;

/**
 * 描述
 * 给定一个仅包含小写字母的字符串，求它的最长回文子串的长度。
 * 所谓回文串，指左右对称的字符串。
 * 所谓子串，指一个字符串删掉其部分前缀和后缀（也可以不删）的字符串
 * 数据范围：字符串长度1≤s≤350
 * 进阶：时间复杂度：O(n) ，空间复杂度：O(n)
 * 输入描述：
 * 输入一个仅包含小写字母的字符串
 * <p>
 * 输出描述：
 * 返回最长回文子串的长度
 *
 *
 * 同类复杂题HJ32密码截取
 */
public class HJ85 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String s = sc.nextLine();

            int max = 0;
            for (int i = 0; i < s.length(); i++) {
                for (int j = s.length(); j >i; j--) {
                    String s1 = s.substring(i, j);
                    if (isPalindromeString(s1)){
                        max = Math.max(max,j-i);
                    }
                }
            }
            System.out.println(max);
        }
    }


    public static boolean isPalindromeString(String s) {
        StringBuilder stringBuilder = new StringBuilder(s);
        return s.equals(stringBuilder.reverse().toString());
    }
}
