package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-16 22:08
 */

import java.util.*;

/**
 * 描述
 * 输入一个只包含小写英文字母和数字的字符串，按照不同字符统计个数由多到少输出统计结果，如果统计的个数相同，则按照ASCII码由小到大排序输出。
 * 数据范围：字符串长度满足 1≤len(str)≤1000
 * <p>
 * 输入描述：
 * 一个只包含小写英文字母和数字的字符串。
 * <p>
 * 输出描述：
 * 一个字符串，为不同字母出现次数的降序表示。若出现次数相同，则按ASCII码的升序输出。
 */
public class HJ102 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String s = sc.nextLine();
            char[] chars = s.toCharArray();
            TreeMap<Character, Integer> map = new TreeMap<>();
            for (int i = 0; i < chars.length; i++) {
                char ch = chars[i];
                map.put(ch, map.getOrDefault(ch, 0) + 1);
            }
            int max = 0;
            for (Integer value : map.values()) {
                if (value > max) {
                    max = value;
                }
            }

            //输出
            while (max > 0) {
                for (Character key : map.keySet()) {
                    if (map.get(key) == max) {
                        System.out.print(key);
                    }
                }
                max--;
            }
        }
    }

}
