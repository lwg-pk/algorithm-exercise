package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-16 11:27
 */

import java.util.Scanner;

/**
 * 二维数组操作
 */
public class HJ83 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            //初始化数组
            int m = sc.nextInt();//行
            int n = sc.nextInt();//列
            int[][] mn = new int[m][n];
            System.out.println((m > 9 || n > 9) ? -1 : 0);
            //交换数据
            int x1 = sc.nextInt();
            int y1 = sc.nextInt();
            int x2 = sc.nextInt();
            int y2 = sc.nextInt();
            if ((x1 < m && x2 < m) && (y1 < n && y2 < n)) {
                System.out.println(0);
            } else {
                System.out.println(-1);
            }
            //输入x ，在第x行上方添加一行。
            int x = sc.nextInt();
            System.out.println((x >= m || (m + 1) > 9) ? -1 : 0);
            //输入y ，在第y列左侧添加一列。
            int y = sc.nextInt();
            System.out.println((y >= n || (n + 1) > 9) ? -1 : 0);
            //输入x 、y ，查找坐标为(x,y) 的单元格的值。
            x = sc.nextInt();
            y = sc.nextInt();
            System.out.println((x >= m || y >= n) ? -1 : 0);
        }
    }

}
