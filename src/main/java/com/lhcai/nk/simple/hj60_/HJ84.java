package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-16 11:46
 */

import java.util.Scanner;

/**
 * 描述
 * 找出给定字符串中大写字符(即'A'-'Z')的个数。
 * 数据范围：字符串长度：1≤∣s∣≤250
 * 字符串中可能包含空格或其他字符
 * 进阶：时间复杂度：O(n) ，空间复杂度： O(n)
 * 输入描述：
 * 对于每组样例，输入一行，代表待统计的字符串
 *
 * 输出描述：
 * 输出一个整数，代表字符串中大写字母的个数
 */
public class HJ84 {
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        while (sc.hasNextLine()){

            String s = sc.nextLine();
            String s1 = s.replaceAll("[^A-Z]", "");

            System.out.println(s1.length());

        }
    }
}
