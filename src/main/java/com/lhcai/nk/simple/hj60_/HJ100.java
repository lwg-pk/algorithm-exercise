package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-16 22:00
 */

import java.util.Scanner;

/**
 * 描述
 * 等差数列 2，5，8，11，14。。。。
 * （从 2 开始的 3 为公差的等差数列）
 * 输出求等差数列前n项和
 * 数据范围：1≤n≤1000
 * 输入描述：
 * 输入一个正整数n。
 * 输出描述：
 * 输出一个相加后的整数。
 */
public class HJ100 {
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        while (sc.hasNextInt()){
            int n = sc.nextInt();
            int num = 0;
            int nums =0;
            for (int i = 0; i < n; i++) {
                    num = 3*i+2;
                   nums += num;
            }
            System.out.println(nums);
        }
    }
}
