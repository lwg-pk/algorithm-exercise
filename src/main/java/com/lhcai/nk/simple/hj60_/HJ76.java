package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-16 10:25
 */

import javax.sound.midi.Soundbank;
import java.util.Scanner;

/**
 * 验证尼科彻斯定理，即：任何一个整数m的立方都可以写成m个连续奇数之和。
 * <p>
 * 例如：
 * <p>
 * 1^3=1
 * <p>
 * 2^3=3+5
 * <p>
 * 3^3=7+9+11
 * <p>
 * 4^3=13+15+17+19
 * <p>
 * 5^3=21+23+25+27+29
 * <p>
 * 6^3=31+33+35+37+39+41
 * <p>
 * 7^3=43+45+47+49+51+53+55
 * <p>
 * 8^3=57+59+61+63+65+67+69+71
 * 输入一个正整数m（m≤100），将m的立方写成m个连续奇数之和的形式输出。
 * 数据范围：1≤m≤100
 * 进阶：时间复杂度：O(m) ，空间复杂度：O(1)
 */
public class HJ76 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            int m = sc.nextInt();
            int nums = m * m - m + 1;

            for (int i = 0; i < m; i++) {
                if (nums == m  * m + m - 1 ){
                    System.out.print(nums);
                    continue;
                }
                System.out.print(nums + "+");
                nums+=2;
            }
        }
        System.out.println();
    }
}
