package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-16 21:13
 */

import java.util.Scanner;

/**
 * 描述
 * 首先输入要输入的整数个数n，然后输入n个整数。输出为n个整数中负数的个数，和所有正整数的平均值，结果保留一位小数。
 * 0即不是正整数，也不是负数，不计入计算。如果没有正数，则平均值为0。
 * <p>
 * 数据范围： 1≤n ≤2000  ，输入的整数都满足 ∣val∣≤1000
 * 输入描述：
 * 首先输入一个正整数n，
 * 然后输入n个整数。
 * <p>
 * 输出描述：
 * 输出负数的个数，和所有正整数的平均值。
 */
public class HJ97 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int len = sc.nextInt();
            int countF = 0, countZ = 0, nums = 0;
            for (int i = 0; i < len; i++) {
                int num = sc.nextInt();
                if (num< 0) {
                    countF++;
                } else if (num > 0){
                    nums += num;
                    countZ++;
                }
            }

            double average = countZ == 0 ? 0.0 : nums * 1.0 / countZ;
            System.out.println(countF + " " + String.format("%.1f", average));

        }
    }

}
