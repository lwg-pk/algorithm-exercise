package com.lhcai.nk.simple.hj60_;

/**
 * @author lhcstart
 * @create 2023-02-16 21:40
 */

import java.util.Scanner;
import java.util.zip.CheckedOutputStream;

/**
 * 描述
 * 自守数是指一个数的平方的尾数等于该数自身的自然数。例如：25^2 = 625，76^2 = 5776，9376^2 = 87909376。请求出n(包括n)以内的自守数的个数
 * 数据范围：1≤n≤10000
 * <p>
 * 输入描述：
 * int型整数
 * <p>
 * 输出描述：
 * n以内自守数的数量。
 */
public class HJ99 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            int n = sc.nextInt();
            int count = 0;


            for (int i = 0; i <= n; i++) {
                if (i == 0 || i == 1) {
                    count++;
                    continue;
                }

                if (solution(i)) {
                    count++;
                }
            }

            System.out.println(count);
        }
    }

    public static boolean solution(int n) {
        String s = String.valueOf(n);
        int shortl = s.length();
        int nums = n * n;
        String s1 = String.valueOf(nums);
        int longl = s1.length();
        if (s1.substring(longl - shortl, longl).equals(s)) {
            return true;
        } else {
            return false;
        }
    }
}

/**
 * //调用api*****
 *
 * endsWith() 方法用于测试字符串是否以指定的后缀结束。
 *
 * public class Main {
 *     public static void main(String[] args) {
 *         Scanner in = new Scanner(System.in);
 *         while (in.hasNext()) {
 *             int n = in.nextInt();
 *             int count = 0;
 *             for (int i = 0; i <= n; i++) {
 *                 String str = String.valueOf(i*i);
 *                 String s = String.valueOf(i);
 *                 if (str.endsWith(s)) {
 *                     count++;
 *                 }
 *             }
 *             System.out.println(count);
 *         }
 *     }
 * }
 */
