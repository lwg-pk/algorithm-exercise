package com.lhcai.haweiOD;

/**
 * @author lhcstart
 * @create 2023-03-01 16:31
 */


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

/**
 * 打印文件
 * <p>
 * 有5台打印机打印文件，每台打印机有自己的待打印队列。
 * 因为打印的文件内容有轻重缓急之分，所以队列中的文件有1~10不同的优先级，其中数字越大优先级越高。
 * 打印机会从自己的待打印队列中选择优先级最高的文件来打印。
 * 如果存在两个优先级一样的文件，则选择最早进入队列的那个文件。
 * 现在请你来模拟这5台打印机的打印过程。
 * <p>
 * 输入描述
 * 每个输入包含1个测试用例，每个测试用例第1行给出发生事件的数量  N(0<N<1000)。
 * 接下来有  N 行，分别表示发生的事件。
 * 共有如下两种事件：
 * IN P NUM，表示有一个拥有优先级 NUM 的文件放到了打印机 P 的待打印队列中。 (0<P≤5,0<NUM≤10)；
 * OUT P，表示打印机 P 进行了一次文件打印，同时该文件从待打印队列中取出。 (0<P≤5)。
 * <p>
 * 输出描述
 * 对于每个测试用例，每次OUT P事件，请在一行中输出文件的编号。
 * 如果此时没有文件可以打印，请输出NULL。
 * 文件的编号定义为：IN P NUM事件发生第 X 次，此处待打印文件的编号为 X。编号从1开始。
 */
public class HJ182 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();//获取事件数量
        sc.nextLine();
        String[] jobs = new String[n];
        for (int i = 0; i < n; i++) {
            jobs[i] = sc.nextLine();
        }
        solve(jobs);

    }

    //子函数
    public static void solve(String[] jobs) {

        HashMap<String, List<int[]>> printers = new HashMap<>();

        for (int i = 0; i < jobs.length; i++) {
            String[] split = jobs[i].split(" ");

            String op = split[0];//获取是打印还是输入
            String num = split[1];//打印机编号
            if (op.equals("IN")) {
                int p = Integer.parseInt(split[2]);
                in(printers, num, i + 1, p);
            } else {
                String res = out(printers, num);
                System.out.println(res);
            }
        }


    }

    private static String out(HashMap<String, List<int[]>> printers, String num) {
        String res = "NULL";
        if (!printers.containsKey(num)) {
            return res;
        }

        List<int[]> jobList = printers.get(num);
        if (jobList.size() == 0) {
            return res;
        }

        jobList.sort((o1, o2) -> {
            return o2[1] - o1[1]; //降序排列
        });

        int seq = jobList.get(0)[0];
        jobList.remove(0);

        return seq + "";
    }

    /**
     * @param printers 存放打印的事件的映射关系
     * @param num
     * @param seq      序列号,放入事件的编号
     * @param p
     */
    private static void in(HashMap<String, List<int[]>> printers, String num, int seq, int p) {
        if (!printers.containsKey(num)) {
            printers.put(num, new ArrayList<>());
        }
        List<int[]> jobList = printers.get(num);
        jobList.add(new int[]{seq, p});
    }
}
