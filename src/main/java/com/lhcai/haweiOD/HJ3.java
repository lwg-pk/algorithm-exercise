package com.lhcai.haweiOD;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * @author lhcstart
 * @create 2023-03-03 15:09
 */

/**
 * 过河问题
 */
public class HJ3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int N = sc.nextInt();//士兵数
            int T = sc.nextInt();//敌军到达时长

            ArrayList<Integer> list = new ArrayList<>();
            for (int i = 0; i < N; i++) {
                list.add(sc.nextInt());
            }
            Collections.sort(list);

            int count = 0;
            int max = 0;
            int idx = 0;
            while (max < T) {
                for (int i = 0; i < list.size(); i++) {
                    if (max <= T) {
                        max += list.get(i);
                        count++;
                        idx = i;
                    }
                }
            }
            if (max > T) {
                max -= list.get(idx);
                count--;
            }

            System.out.println(count + " " + max);
        }
    }

}
