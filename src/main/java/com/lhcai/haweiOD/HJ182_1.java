package com.lhcai.haweiOD;

import java.sql.SQLClientInfoException;
import java.util.*;

/**
 * @author lhcstart
 * @create 2023-03-03 10:50
 */
public class HJ182_1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int n = sc.nextInt();
            sc.nextLine();
            String[] jobs = new String[n];
            for (int i = 0; i < n; i++) {
                jobs[i] = sc.nextLine();
            }
            solve(jobs);
        }
    }

    public static void solve(String[] jobs) {
        HashMap<String, List<int[]>> map = new HashMap<>();

        for (int i = 0; i < jobs.length; i++) {
            String[] split = jobs[i].split(" ");
            String op = split[0];
            String p = split[1];
            if (op == "IN") {
                int num = Integer.parseInt(split[2]);
                in(map, p, i + 1, num);
            } else {
                String res = out(map, p);
                System.out.println(res);
            }
        }

    }

    public static String out(HashMap<String, List<int[]>> map, String p) {
        String res = "NULL";

        if (!map.containsKey(p)) {
            return res;
        }

        List<int[]> jobList = map.get(p);
        if (jobList.size() == 0) {
            return res;
        }

        jobList.sort((o1, o2) -> {
            return o2[1] - o1[1];
        });

        int seq = jobList.get(0)[0];
        jobList.remove(0);
        return seq + "";
    }

    private static void in(HashMap<String, List<int[]>> map, String p, int seq, int num) {
        if (!map.containsKey(p)) {
            map.put(p, new ArrayList<>());
        }

        List<int[]> jobList = map.get(p);
        jobList.add(new int[]{seq, num});

    }
}
