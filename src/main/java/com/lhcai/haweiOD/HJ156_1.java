package com.lhcai.haweiOD;

import java.util.*;

/**
 * @author lhcstart
 * @create 2023-03-03 10:18
 */
public class HJ156_1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int m = sc.nextInt();
            int n = sc.nextInt();
            sc.nextLine();

            int[][] arr = new int[m][n];
            int[] sr = new int[2];
            int[] dst = new int[2];
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    arr[i][j] = sc.nextInt();
                    if (arr[i][j] > 0) {
                        sr[0] = i;
                        sr[1] = j;
                    }
                }
            }
            dst[0] = sc.nextInt();
            dst[1] = sc.nextInt();
            int solve = solve(arr, sr, dst);
            System.out.println(solve);

        }
    }

    static LinkedList<Block> queue = new LinkedList();

    public static int solve(int[][] arr, int[] sr, int[] dst) {

        int x = sr[0];
        int y = sr[1];
        queue.add(new Block(x, y, arr[x][y]));

        while (queue.size() > 0) {
            Block block = queue.removeFirst();
            dfs(arr, block.x, block.y, block.d);
        }

        return arr[dst[0]][dst[1]];

    }

    static int[][] dis = new int[][]{{1, 0}, {-1, 0}, {0, 1}, {0, -1}};

    public static void dfs(int[][] arr, int x, int y, int d) {
        for (int[] di : dis) {
            int newX = di[0] + x, newY = di[1] + y;
            if (newX >= 0 && newY >= 0 && newX < arr.length && newY < arr[0].length) {

                int next = arr[newX][newY];
                if (next == 0) {
                    arr[newX][newY] = d - 1;
                }

                if (d > 2 && next != -1) {
                    queue.add(new Block(newX, newY, d - 1));
                }
            }
        }
    }

    public static class Block {
        int x;
        int y;
        int d;

        public Block(int x, int y, int d) {
            this.x = x;
            this.y = y;
            this.d = d;
        }
    }
}
