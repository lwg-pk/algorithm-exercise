package com.lhcai.haweiOD;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * @author lhcstart
 * @create 2023-03-03 15:09
 */
public class HJ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int N = sc.nextInt();//士兵数
            int T = sc.nextInt();//敌军到达时长

            ArrayList<Integer> list = new ArrayList<>();
            for (int i = 0; i < N; i++) {
                list.add(sc.nextInt());
            }


            ArrayList<Integer> res = new ArrayList<>();
            int max = 0;
            while (list.size() > 0 && max <= T) {
                max = dfs(list, max, T, res);
            }
            System.out.println(res.size() + " " + max);
        }
    }


    public static int dfs(ArrayList<Integer> list, int max, int T, ArrayList<Integer> res) {

        //每次过河为士兵排序
        Collections.sort(list);//未过河的

        //1、未过河,最小的过河
        Integer f2 = list.remove(1);
        Integer f1 = list.remove(0);
        //计算过河时间
        max += Math.max(f1, f2);


        //加入过河队列
        res.add(f1);
        res.add(f2);
        Collections.sort(res);//过河的排序

        //计算返回时间
        max += Math.min(res.get(0), res.get(1));
        if (max > T) {
            return max -= Math.min(res.get(0), res.get(1));
        }

        //2、最快的回去
        Integer mast = res.remove(0);
        //加入未过河队列
        list.add(mast);

        //
        if (list.size() <= 2) {
            max += Math.max(list.get(0), list.get(1));
            res.add(list.remove(1));
            res.add(list.remove(0));
        }

        if (list.size() > 2) {
            //3、最慢的过河
            int l2 = list.remove(list.size() - 2);
            int l1 = list.remove(list.size() - 1);
            max += Math.max(l1, l2);

            //加入过河队列
            res.add(l1);
            res.add(l2);
            Collections.sort(res);

            //计算返回时间
            max += Math.min(res.get(0), res.get(1));

            //最快的回去
            Integer mast2 = res.remove(0);
            //加入未过河队列
            list.add(mast);
        }

        return max;
    }
}
