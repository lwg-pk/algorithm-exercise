package com.lhcai.haweiOD;

import java.util.HashMap;
import java.util.Scanner;

/**
 * @author lhcstart
 * @create 2023-03-03 14:59
 */

/**
 * 滑动窗口
 */
public class HJ2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String s = sc.nextLine();
            String[] split = s.split(" ");
//            sc.nextLine();//去除空格
            int n = Integer.parseInt(sc.nextLine());

            if (n == 0 || split.length == 0) {
                System.out.println(0);
                return;
            }


            int max = 0;
            for (int i = 0; i < (split.length - n + 1); i++) {
                HashMap<String, Integer> map = new HashMap<>();

                for (int j = i; j < i + n; j++) {
                    String str = split[j];
                    map.put(str, map.getOrDefault(str, 0) + 1);
                }

                for (Integer value : map.values()) {
                    if (value >= max) {
                        max = value;
                    }
                }
            }
            System.out.println(max);

        }
    }
}
