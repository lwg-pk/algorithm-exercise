package com.lhcai.haweiOD;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * @author lhcstart
 * @create 2023-03-03 14:43
 */
public class HJ1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while (sc.hasNext()) {
            int n = sc.nextInt();

            HashMap<Integer, Integer> map = new HashMap<>();
            ArrayList<Integer> list = new ArrayList<>();

            for (int i = 0; i < n; i++) {
                int a = sc.nextInt();
                if (!map.containsKey(a)) {
                    map.put(a, i);
                } else {
                    Integer idx = map.get(a);
                    int len = i - idx;
                    list.add(len);
                }
            }
            if (map.size() == n) {
                System.out.println(-1);
            }

            int max = 0;
            for (Integer it : list) {
                if (it > max) {
                    max = it;
                }
            }

            System.out.println(max);
        }
    }
}
