//package com.lhcai.haweiOD;
//
///**
// * @author lhcstart
// * @create 2023-03-01 16:19
// */
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Scanner;
//
///**
// * AI处理器组合
// * 某公司研发了一款高性能AI处理器。每台物理设备具备8颗AI处理器，编号分别为0、1、2、3、4、5、6、7。编号0-3的处理器处于同一个链路中，编号4-7的处理器处于另外一个链路中，不通链路中的处理器不能通信，如下图所示。现给定服务器可用的处理器编号数组array，以及任务申请的处理器数量num，找出符合下列亲和性调度原则的芯片组合。如果不存在符合要求的组合，则返回空列表。
// * 亲和性调度原则：
// * <p>
// * 如果申请处理器个数为1，则选择同一链路，剩余可用的处理器数量为1个的最佳，其次是剩余3个的为次佳，然后是剩余2个，最后是剩余4个。
// * 如果申请处理器个数为2，则选择同一链路剩余可用的处理器数量2个的为最佳，其次是剩余4个，最后是剩余3个。
// * 如果申请处理器个数为4，则必须选择同一链路剩余可用的处理器数量为4个。
// * 如果申请处理器个数为8，则申请节点所有8个处理器。
// * 提示：
// * 任务申请的处理器数量只能是1、2、4、8
// * 编号0-3的处理器处于一个链路，编号4-7的处理器处于另外一个链路。
// * 处理器编号唯一，且不存在相同编号处理器
// */
//public class HJ221 {
//    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        while (sc.hasNext()) {
//
//            String str = sc.nextLine().replace("[", "");
//            str = str.replace(",", "");
//            str = str.replace("]", "");
//            str = str.replace(" ", "");
//
//            Integer[] arr = new Integer[str.length()];
//            for (int i = 0; i < str.length(); i++) {
//                arr[i] = str.charAt(i) - '0';
//            }
//
//            String num = sc.next();
//            System.out.println(solve(arr,num));
//        }
//    }
//
//    public static String solve(Integer[] arr, String num) {
//
//        ArrayList<Integer> link1 = new ArrayList<>();
//        ArrayList<Integer> link2 = new ArrayList<>();
//
//        Arrays.sort(arr);//升序排列
//
//        //服务器存入链路中
//        for (Integer e : arr) {
//            if (e < 4) {
//                link1.add(e);
//            } else {
//                link2.add(e);
//            }
//        }
//
//        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
//        int len1 = link1.size();//链路一可用服务器
//        int len2 = link2.size();//链路二可用服务器
//
//        switch (num) {
//            case "1":
//                if (len1 == 1 || len2 == 1) {
//                    if (len1 == 1) dfs(link1, 0, 1, new ArrayList<>(), res);
//                    if (len2 == 1) dfs(link1, 0, 1, new ArrayList<>(), res);
//
//                } else if (len1 == 3 || len2 == 3) {
//                    if (len1 == 3) dfs(link1, 0, 1, new ArrayList<>(), res);
//                    if (len2 == 3) dfs(link1, 0, 1, new ArrayList<>(), res);
//
//                } else if (len1 == 2 || len2 == 2) {
//                    if (len1 == 2) dfs(link1, 0, 1, new ArrayList<>(), res);
//                    if (len2 == 2) dfs(link1, 0, 1, new ArrayList<>(), res);
//
//                } else if (len1 == 4 || len2 == 4) {
//                    if (len1 == 4) dfs(link1, 0, 1, new ArrayList<>(), res);
//                    if (len2 == 4) dfs(link1, 0, 1, new ArrayList<>(), res);
//                }
//                break;
//            case "2":
//                if (len1 == 2 || len2 == 2) {
//                    if (len1 == 2) dfs(link1, 0, 2, new ArrayList<>(), res);
//                    if (len2 == 2) dfs(link2, 0, 2, new ArrayList<>(), res);
//
//                } else if (len1 == 4 || len2 == 4) {
//                    if (len1 == 4) dfs(link1, 0, 2, new ArrayList<>(), res);
//                    if (len2 == 4) dfs(link2, 0, 2, new ArrayList<>(), res);
//
//                } else if (len1 == 3 || len2 == 3) {
//                    if (len1 == 3) dfs(link1, 0, 2, new ArrayList<>(), res);
//                    if (len2 == 3) dfs(link2, 0, 2, new ArrayList<>(), res);
//                }
//                break;
//            case "4":
//                if (len1 == 4 || len2 == 4) {
//                    if (len1 == 4) res.add(link1);
//                    if (len2 == 4) res.add(link2);
//                }
//                break;
//            case "8":
//                ArrayList<Integer> link = new ArrayList<>();
//                for (Integer e : arr) {
//                    link.add(e);
//                }
//                res.add(link);
//        }
//        return res.toString();
//    }
//
//    /**
//     * @param arr   链路
//     * @param index 链路中处理器的索引，从0开始
//     * @param level 申请的处理器的数量
//     * @param path  空白list存放可能的处理器
//     * @param res   结果集
//     */
//    public static void dfs(ArrayList<Integer> arr, int index,
//                           int level,
//                           ArrayList<Integer> path,
//                           ArrayList<ArrayList<Integer>> res) {
//
//        //符合条件
//        if (path.size() == level) {
//            res.add(new ArrayList<>(path));//克隆放入结果集
//        }
//        for (int i = index; i < arr.size(); i++) {
//
//            path.add(arr.get(i));
//            dfs(arr, i + 1, level, path, res);
//            path.remove(path.size() - 1);
//        }
//    }
//}
